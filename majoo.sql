-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 08, 2021 at 10:35 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `majoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `nama_admin` text NOT NULL,
  `level` enum('super admin','admin','karyawan','') NOT NULL,
  `list_menu` text NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `status_delete` int(11) NOT NULL,
  `delete_from` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `nama_admin`, `level`, `list_menu`, `create_by`, `update_by`, `status_delete`, `delete_from`, `date_created`, `date_updated`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'PT. Majoo', 'super admin', '1,2,3', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pemesanan`
--

CREATE TABLE `detail_pemesanan` (
  `id` int(11) NOT NULL,
  `id_pemesanan` int(11) NOT NULL,
  `nama_item` text NOT NULL,
  `jml` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `status_delete` int(11) NOT NULL,
  `delete_from` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pemesanan`
--

INSERT INTO `detail_pemesanan` (`id`, `id_pemesanan`, `nama_item`, `jml`, `harga`, `create_by`, `update_by`, `status_delete`, `delete_from`, `date_created`, `date_updated`) VALUES
(1, 1, 'Majoo PRO', 1, 2750000, 1, 0, 0, 0, '2021-09-09 02:24:07', '0000-00-00 00:00:00'),
(2, 1, 'majoo Advance', 2, 2750000, 1, 0, 0, 0, '2021-09-09 02:24:07', '0000-00-00 00:00:00'),
(3, 2, 'Majoo PRO', 2, 2750000, 0, 0, 0, 0, '2021-09-09 03:01:09', '0000-00-00 00:00:00'),
(4, 2, 'majoo Advance', 2, 2750000, 0, 0, 0, 0, '2021-09-09 03:01:09', '0000-00-00 00:00:00'),
(5, 3, 'Majoo PRO', 2, 2750000, 1, 0, 0, 0, '2021-09-09 03:17:33', '0000-00-00 00:00:00'),
(7, 4, 'majoo Advance', 3, 2750000, 1, 0, 0, 0, '2021-09-09 03:29:38', '0000-00-00 00:00:00'),
(8, 4, 'majoo Lifestyle', 1, 2750000, 1, 0, 0, 0, '2021-09-09 03:29:38', '0000-00-00 00:00:00'),
(9, 4, 'majoo Desktop', 1, 2750000, 1, 0, 0, 0, '2021-09-09 03:29:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `nama` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `nama`) VALUES
(1, 'dashboard'),
(2, 'manage pemesanan'),
(3, 'manage produk');

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id` int(11) NOT NULL,
  `kode_pesan` text NOT NULL,
  `atas_nama` text NOT NULL,
  `no_hp` int(11) NOT NULL,
  `email` text NOT NULL,
  `grand_total` int(11) NOT NULL,
  `status_konfirmasi` tinyint(1) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `status_delete` int(11) NOT NULL,
  `delete_from` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemesanan`
--

INSERT INTO `pemesanan` (`id`, `kode_pesan`, `atas_nama`, `no_hp`, `email`, `grand_total`, `status_konfirmasi`, `create_by`, `update_by`, `status_delete`, `delete_from`, `date_created`, `date_updated`) VALUES
(1, 'Ilham_Burhanuddin_Rabbani_090921', 'Ilham Burhanuddin Rabbani', 2147483647, 'mail@g.com', 8250000, 0, 1, 0, 0, 0, '2021-09-09 02:24:07', '0000-00-00 00:00:00'),
(2, 'ilham_090921', 'ilham', 2147483647, 'ilhambr05@gmail.com', 11000000, 0, 0, 0, 0, 0, '2021-09-09 03:01:09', '0000-00-00 00:00:00'),
(3, 'ilham_lagi_090921', 'ilham lagi', 2147483647, 'ilhambr05@gmail.com', 5500000, 0, 1, 0, 0, 0, '2021-09-09 03:17:33', '0000-00-00 00:00:00'),
(4, 'Fulan_090921', 'Fulan', 2147483647, 'e@m.com', 13750000, 0, 1, 0, 0, 0, '2021-09-09 03:29:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(11) NOT NULL,
  `foto` text NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `status_delete` int(11) NOT NULL,
  `delete_from` int(11) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `nama`, `deskripsi`, `harga`, `foto`, `create_by`, `update_by`, `status_delete`, `delete_from`, `date_created`, `date_updated`) VALUES
(1, 'Majoo PRO', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam', 2750000, '440029f4c67768c170e4faa187d899de.png', 1, 0, 0, 0, '2021-09-08 22:33:39', '0000-00-00 00:00:00'),
(2, 'majoo Advance', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, ', 2750000, 'c06a81eb8dd33693457dc5cfa8060326.png', 1, 0, 0, 0, '2021-09-08 22:35:46', '0000-00-00 00:00:00'),
(3, 'majoo Lifestyle', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 2750000, '25dbc2ed3768b3ed832c10e8e8d4ad6e.png', 1, 0, 0, 0, '2021-09-09 03:24:40', '0000-00-00 00:00:00'),
(4, 'majoo Desktop', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 2750000, 'aa2dfff467740b038c1f07d30f09d042.png', 1, 0, 0, 0, '2021-09-09 03:25:12', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pemesanan`
--
ALTER TABLE `detail_pemesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `detail_pemesanan`
--
ALTER TABLE `detail_pemesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
