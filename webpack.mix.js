let mix = require('laravel-mix');

// library npm untuk panggil variable versioning dari .env
// require('dotenv').config({ path: 'env.version' });
require('dotenv').config();


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

// mix.setPublicPath('./'); // Uncomment this if you use windows environment, don't forget to set your npm global binary location into your Environment System Path.
mix
	.combine([
		'resource/js/notify.min.js'
	], 'public/mix_assets/js/notify.min.js')
  	.combine([
		'node_modules/socket.io-client/dist/socket.io.js'
	], 'public/mix_assets/js/socket.io.js')
  	.combine([
	  'resource/js/manage_region.js'
		  ], 'public/mix_assets/js/manage_region' + '_' + process.env.MANAGE_REGION + '.js')
	.combine([
		'resource/js/manage_store.js'
	], 'public/mix_assets/js/manage_store' + '_' + process.env.MANAGE_STORE + '.js')
	.combine([
		'resource/js/manage_voucher.js'
	], 'public/mix_assets/js/manage_voucher' + '_' + process.env.MANAGE_VOUCHER + '.js')
	.combine([
		'resource/js/manage_pemesanan.js'
	], 'public/mix_assets/js/manage_pemesanan' + '_' + process.env.MANAGE_PEMESANAN + '.js')
	.combine([
		'resource/js/manage_detail_pemesanan.js'
	], 'public/mix_assets/js/manage_detail_pemesanan' + '_' + process.env.MANAGE_DETAIL_PEMESANAN + '.js')
	.combine([
		'resource/js/manage_informasi.js'
	], 'public/mix_assets/js/manage_informasi' + '_' + process.env.MANAGE_INFORMASI + '.js')
	.combine([
		'resource/js/manage_testimoni.js'
	], 'public/mix_assets/js/manage_testimoni' + '_' + process.env.MANAGE_TESTIMONI + '.js')
	.combine([
		'resource/js/manage_slider.js'
	], 'public/mix_assets/js/manage_slider' + '_' + process.env.MANAGE_SLIDER + '.js')
	.combine([
		'resource/js/list_member.js'
	], 'public/mix_assets/js/list_member' + '_' + process.env.LIST_MEMBER + '.js')
	.combine([
		'resource/js/dashboard.js'
	], 'public/mix_assets/js/dashboard' + '_' + process.env.DASHBOARD + '.js')
	.combine([
		'resource/js/manage_email_promotion.js'
	], 'public/mix_assets/js/manage_email_promotion' + '_' + process.env.MANAGE_EMAIL_PROMOTION + '.js')
	.combine([
		'resource/js/setting.js'
	], 'public/mix_assets/js/setting' + '_' + process.env.SETTING + '.js')
	.combine([
		'resource/js/manage_pemesanan_produk.js'
	], 'public/mix_assets/js/manage_pemesanan_produk' + '_' + process.env.MANAGE_PEMESANAN_PRODUK + '.js')
	.combine([
		'resource/js/manage_detail_pemesanan_produk.js'
	], 'public/mix_assets/js/manage_detail_pemesanan_produk' + '_' + process.env.MANAGE_DETAIL_PEMESANAN_PRODUK + '.js')
	.combine([
		'resource/js/manage_pemesanan_treatment.js'
	], 'public/mix_assets/js/manage_pemesanan_treatment' + '_' + process.env.MANAGE_PEMESANAN_TREATMENT + '.js')
	.combine([
		'resource/js/manage_detail_pemesanan_treatment.js'
	], 'public/mix_assets/js/manage_detail_pemesanan_treatment' + '_' + process.env.MANAGE_DETAIL_PEMESANAN_TREATMENT + '.js')
	.combine([
		'resource/js/profil_member.js'
	], 'public/mix_assets/js/profil_member' + '_' + process.env.PROFIL_MEMBER + '.js')
	.combine([
		'resource/js/profile.js'
	], 'public/mix_assets/js/profile' + '_' + process.env.PROFILE + '.js')
  .combine([
    'resource/js/pengadaan_produk.js'
  ], 'public/mix_assets/js/pengadaan_produk' + '_' + process.env.PENGADAAN_PRODUK + '.js')
  .combine([
    'resource/js/manage_karyawan.js'
  ], 'public/mix_assets/js/manage_karyawan' + '_' + process.env.MANAGE_KARYAWAN + '.js')
  .combine([
    'resource/js/manage_admin.js'
  ], 'public/mix_assets/js/manage_admin' + '_' + process.env.MANAGE_ADMIN + '.js')
  .combine([
    'resource/js/manage_level_member.js'
  ], 'public/mix_assets/js/manage_level_member' + '_' + process.env.MANAGE_LEVEL_MEMBER + '.js')
  .combine([
    'resource/js/gallery.js'
  ], 'public/mix_assets/js/gallery' + '_' + process.env.GALLERY + '.js')
  .combine([
    'resource/js/user_message.js'
  ], 'public/mix_assets/js/user_message' + '_' + process.env.USER_MESSAGE + '.js')
  .combine([
      'resource/js/js-ori.js',
      'resource/js/checkout.js',
      'resource/js/store-checkout.js'
    ], 'public/mix_assets/js/js_mix' + '_' + process.env.JS_MIX + '.js')

  //DONE MANUAL VERSIONING

	.sass('resource/css/style-material.scss', 'public/mix_assets/css/style-material' + '_' + process.env.STYLE_MATERIAL + '.css')
  .sass('resource/css/style_ori.scss', 'public/mix_assets/css/style_ori' + '_' + process.env.STYLE_ORI + '.css')
    .options({
      processCssUrls: false
    })
  .copyDirectory('resource/img', 'public/mix_assets/img')
  .copyDirectory('resource/sound', 'public/mix_assets/sound');
    /**
     * If you want to separate your javascript from main javascript app.js you can use combine to combine multiple javascript into another file. Please reorder as your need.
     */
    // .combine([
    //    'template.js',
    //     'template.all.min.js'
    // ], 'assets/js/vendor.js');


    if (mix.inProduction()) {
        mix.options({
          uglify: {
            uglifyOptions: {
              warnings: false,
              comments: false,
              beautify: false,
              compress: {
                drop_console: true,
              }
            }
          }
        });
    }
