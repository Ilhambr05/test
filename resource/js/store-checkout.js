// beberapa value untuk kegunaan script ini harus di set sebelumnya di file berbeda.

function set_rasio_img(){
	// var w = $(".img_item").width();
	// $(".img_item, .detail-btn, .ket_jml").css({ 'height': w + "px" });

	// var w = $("#list_cart img").width();
	// $("#list_cart img").css({ 'height': w + "px" });
}

var waitForFinalEvent = (function () { 
	//delay sampai event selesai execute
	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) {
			uniqueId = "Don't call this twice without a uniqueId";
		}
		if (timers[uniqueId]) {
			clearTimeout (timers[uniqueId]);
		}
		timers[uniqueId] = setTimeout(callback, ms);
	};
})();

$(window).resize(function () { 
	// panggil ketika screen size berubah
	waitForFinalEvent(function(){
		set_rasio_img();
	}, 500, "win resize selesai");
});

var moneyFormat = wNumb({
	mark: ',',
	decimals: 0,
	thousand: '.',
	prefix: '',
	suffix: ''
});

$('a[data-toggle="pill"]').on('hide.bs.tab', function (e) {
	//scroll ke atas jika pill di tekan.
	$('html, body').animate({
        scrollTop: $('.section').offset().top - $('#sticky_navigation').outerHeight()
    }, 0);
	// console.log(e.target); // newly activated tab
	// console.log(e.relatedTarget); // previous active tab
})

var cart_produk = get_data_cart('produk');

$( document ).ready(function() {
	//$.removeCookie('Impressions-cart');

	// panggil di page jika document sudah siap
	// *tak masalah document.ready dipanggil lebih dari sekali di satu page
	init_item_selected();
});

function get_data_cart(tipe){
	if(tipe == 'produk'){
		if(upload_ajax){
			// value cart d ambil dari pengaturan script sebelumnya
			var data_cart = cart_member.produk;

			return data_cart;
		}else{
			if($.cookie('Impressions-cart')){
				return JSON.parse($.cookie('Impressions-cart'));
			}
			else{
				return [];
			}
		}
	}

	else if(tipe == 'treatment'){
		if(upload_ajax){
			// value cart d ambil dari pengaturan script sebelumnya
			var data_cart = cart_member.treatment;

			return data_cart;
		}else{
			if($.cookie('Impressions-cart-treatment')){
				return JSON.parse($.cookie('Impressions-cart-treatment'));
			}
			else{
				return [];
			}
		}
	}
}

function init_item_selected(){
	//membuat tampilan item yang sudah masuk di cart

	var nominal = 0;

	for (var i in cart_produk) {
		var el = $('[data-id="'+cart_produk[i].id+'"][tipe-item="produk"]');
		el.find('.ket_jml').show();
		if (cart_produk[i].jml > 1) {
			el.find('.icon_del').text('exposure_minus_1');
		}
		el.find('.val_jml').text(cart_produk[i].jml);
		$('#modal-cart').find('#total_item_'+cart_produk[i].id).text(cart_produk[i].jml);

		el.find('.btn_beli').hide();
		el.find('.jml_beli').show();

		var harga = el.find('harga').text();
		harga = parseInt(harga.replace(/\./g,''));

		if(cart_produk[i].jml >= cart_produk[i].stok){
			el.find('.jml_beli .btn-success').prop('disabled', true);
		}else{
			el.find('.jml_beli .btn-success').prop('disabled', false);
		}

		nominal += cart_produk[i].jml*harga;
	}

	$('#jml_cart').text(cart_produk.length);

	hit_total();
}

function detail(id, tipe){
	var selector = $('[data-id="'+id+'"][tipe-item="'+tipe+'"]');
	var bg = selector.find('img_item').text();
	bg = bg.replace(/ /g, '%20');

	$('#modal-detail nama').text(selector.find('nama').text());
	$('#modal-detail harga').text(selector.find('harga').text());
	$('#modal-detail satuan').text(selector.find('satuan').text());
	$('#modal-detail promo').text(selector.find('promo').text());
	$('#modal-detail deskripsi').html(selector.find('deskripsi').html());
	$('#modal-detail .img_item').css('background-image', "url('"+selector.find('img_item').text()+"')");

	//tipe produk/treatment
	// var tipe = selector.attr('tipe-item');

	if(tipe == 'produk'){
		//var imgs_produk harus disediakan sebelum script ini
		var id_master_produk = selector.attr('id-master-produk');

		var el = '';
		$.each(imgs_produk[id_master_produk], function( index, value ) {
			if(index > 0){
				// var url = 'url("'+value.path+'")';
				el += 
					'<div class="col-xs-12 col-sm-12 col-md-4">'+
						'<img class="rounded img-raised w-100" src="'+value.path+'"></img>'+
					'</div>';
			}
		});
		console.log(el);
		$('#img_detail').html(el);
	}

	else if(tipe == 'treatment'){
		//var imgs_produk harus disediakan sebelum script ini
		var id_master_treatment = selector.attr('id-master-treatment');

		var el = '';
		$.each(imgs_treatment[id_master_treatment], function( index, value ) {
			// var url = 'url("'+value.path+'")';
			if(index > 0) {
				el += 
					'<div class="col-xs-12 col-sm-12 col-md-4">'+
						'<img class="rounded img-raised w-100" src="'+value.path+'"></img>'+
					'</div>';
			}
		});
		console.log(el);
		$('#img_detail').html(el);
	}

	$('#modal-detail').modal('show');
}

function beli(id_item,btn,tipe){
	$(btn).parent().hide();
	$(btn).parent().siblings('.jml_beli').show();

	add_cart(id_item, tipe);
}

function add_cart(id_item, tipe)
{
	if(upload_ajax)
	{
		// mode add/min/remove
		$.ajax({
	    	url : 'shop/manage_cart',
	    	type: "POST",
	    	data: {
	    		id_item: id_item,
	    		mode: 'add',
	    		tipe: tipe,
	    	},
	    	dataType: "JSON",
	    	success: function(data)
	    	{
	    		if(data.response){
					atur_view_add(id_item, tipe);
	    		}else{
	    			alert('Terjadi Kesalahan.');
	        		// location.reload();
	    		}
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	        	alert('Terjadi Kesalahan server.');
	        	// location.reload();
	        }
	    });
	}
	else
	{
		atur_view_add(id_item, tipe);
	}
}

function atur_view_add(id_item, tipe)
{
	var exist = 0;
	var jml_arr = 1;

	if(tipe == 'produk') {
		var el = $('[data-id="'+id_item+'"][tipe-item="produk"]');

		console.log('selector utama');
		console.log(el);

		el.find('.ket_jml').show();

		for (var i in cart_produk) {
			if (cart_produk[i].id == id_item) {

				jml_arr = parseInt(cart_produk[i].jml) + 1;
				cart_produk[i].jml = jml_arr;
				
				exist = 1;
				el.find('.icon_del').text('exposure_minus_1');

				var nominal = 0;
				nominal += cart_produk[i].jml*cart_produk[i].harga;

				el.find('#total_harga_'+id_item).text(moneyFormat.to(nominal));

				if(jml_arr >= cart_produk[i].stok) {
					// disable button untuk nambah di cart produk
					el.find('.jml_beli .btn-success').prop('disabled', true);

					// disable button untuk nambah di cart produk
					el.find('.handler_jumlah_item_cart .btn-success').prop('disabled', true);
				}

				break; 
				//Stop loop
			}
		}
		
		el.find('.icon_del').text('exposure_minus_1');

		el.find('.val_jml').text(jml_arr);
		el.find('#total_item_'+id_item).text(jml_arr);

		if(!exist) {
			var nama = el.find('nama').text();
			// var image = el.find('img').prop('src');
			var image = el.find('img_item').text();
			var harga = el.find('harga').text();
			var stok = el.find('stok').text();
			var tipe_produk = el.attr('tipe-produk');
			var id_mp = el.attr('id-master-produk');
			harga = parseInt(harga.replace(/\./g,''));

			var obj = {id:id_item, id_mp:id_mp, jml:1, stok:stok, nama:nama, tipe:tipe_produk, harga:harga};

			cart_produk.push(obj);
			console.log('tambah item cart lokal - cookie');
			//tambah obj
		}

		console.log('jml_arr ' + jml_arr);

	} 

	// console.log(jml_arr);

	proses_lanjutan();
}

function cek_status_voucher(id_item, tipe, remove)
{
	var voucher_bisa_digunakan = true;
	// CEK STATUS VOUCHER JIKA QTY ITEM DIKURANGI

	var nominal_cart = 0;

	for (var i in cart_produk) {
		nominal_cart += cart_produk[i].jml*cart_produk[i].harga;
	}

	var minimal_beli = 0;
	var id_voucher = 0;
	for ( var i in voucher_terpilih ) {
		minimal_beli = voucher_terpilih[i].minimal_beli;
		id_voucher = voucher_terpilih[i].id_voucher;
	}

	var value_pengurangan = 0;

	if( tipe == 'produk' ) {
		for (var i in cart_produk) {
			if (cart_produk[i].id == id_item) {

				if(remove){

					value_pengurangan = parseInt(cart_produk[i].jml) * parseInt(cart_produk[i].harga);

				} else {

					value_pengurangan = parseInt(cart_produk[i].harga);

				}
				
				break; 
				//Stop loop
			}
		}
	}
		
	// jika value nominal_cart nantinya kurang dari minimal_beli pada voucher
	var cek = parseInt(nominal_cart) - parseInt(value_pengurangan);
	console.log('nominal ' + cek);
	console.log(minimal_beli);
	if( cek < minimal_beli ){
		var lanjut = confirm( 'Voucher Akan Menjadi Tidak Valid, Lanjutkan ?' );

		if( lanjut ){
			// pilih_voucher(id_voucher, 'batal');
			voucher_bisa_digunakan = false;
		} else {
			voucher_bisa_digunakan = true;
		}
	}

	// console.log(voucher_bisa_digunakan);

	return voucher_bisa_digunakan;
}

function rm_cart(id_item,btn,remove=0, tipe){

	var voucher_bisa_digunakan = cek_status_voucher(id_item, tipe, remove);

	if( upload_ajax )
	{
		// rm_ajax(id_item, btn, remove);
		var mode_rm = 'min';

		if(remove){
			mode_rm = 'remove';
		}

		// mode add/min/remove
		$.ajax({
	    	url : 'shop/manage_cart',
	    	type: "POST",
	    	data: {
	    		id_item: id_item,
	    		mode: mode_rm,
	    		tipe: tipe,
	    	},
	    	dataType: "JSON",
	    	success: function(data)
	    	{
	    		if(data.response){
	    			atur_view_rm(id_item, btn, remove, tipe);
	    		}else{
	    			alert('Terjadi Kesalahan.');
	        		// location.reload();
	    		}
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	        	alert('Terjadi Kesalahan server.');
	        	// location.reload();
	        }
	    }).done( function() {
	    	//kalau proses ajax selesai dan voucher tidak bisa digunakan
	    	if(!voucher_bisa_digunakan){
	    		//batalkan voucher
	    		pilih_voucher(0, 'batal');
	    	}
		});
	}
	else
	{
		atur_view_rm(id_item, btn, remove, tipe);
	}
}

function atur_view_rm(id_item, btn, remove, tipe)
{
	var hide = 0;
	var jml_arr = 1;

	// console.log('remove '+remove);
	if( tipe == 'produk' ) {

		var el = $('[data-id="'+id_item+'"][tipe-item="produk"]');
		for (var i in cart_produk) {
			if (cart_produk[i].id == id_item) {
				
				cart_produk[i].jml = jml_arr = parseInt(cart_produk[i].jml) - 1;
				// jml_arr = cart_produk[i].jml;

				var nominal = 0;

				nominal += parseInt(cart_produk[i].jml) * parseInt(cart_produk[i].harga);

				if(remove){
					cart_produk[i].jml = 0; 
					hide = 1;
				}
				if(jml_arr <= 1){
					el.find('.icon_del').text('highlight_off');
				}
				if(jml_arr <= 0){
					//remove arr index
					cart_produk.splice(i, 1);
					hide = 1;
					jml_arr = 0;
					$(btn).closest('.card-cart').remove();
				}

				el.find('#total_harga_'+id_item).text(moneyFormat.to(nominal));
				
				break; 
				//Stop loop
			}
		}		

		el.find('.val_jml').text(jml_arr);
		el.find('#total_item_'+id_item).text(jml_arr);
		
		if(hide){
			el.find('.ket_jml, .jml_beli').hide();
			$(btn).closest('.card-cart').remove();
			el.find('.btn_beli').show();
		}

		if(jml_arr != el.find('stok').text()){
			// un-disable button untuk nambah di list produk
			el.find('.jml_beli .btn-success').prop('disabled', false);

			// un-disable button untuk nambah di cart produk
			el.find('.handler_jumlah_item_cart .btn-success').prop('disabled', false);
		}

	}

	proses_lanjutan();
}

function proses_lanjutan(){
	//masukkan ke cookie (jika belum login)
	if(!upload_ajax)
	{
		$.cookie('Impressions-cart', JSON.stringify(cart_produk), { path: '/' });
	}
	$('#jml_cart').text(cart_produk.length);

	hit_total();

	//jika isi cart kosong
	if(!cart_produk.length){
		$('#modal-cart').modal('hide');
	}
}

function hit_total()
{
	var nominal = 0;
	var total_potongan = 0;
	for (var i in cart_produk) {
		nominal += cart_produk[i].jml*cart_produk[i].harga;
	}

	$('.total_cart_sebelum_potongan').text(moneyFormat.to(nominal));

	var nominal_potongan = 0;
	// hitung potongan
	for ( var i in voucher_terpilih ) {
		
		// tipe voucher -> Nominal/Persen
        // jika persen -> yg d pakai colmn diskon_persen & max_diskon
        // jika nominal -> yg d pakai colmn nominal_diskon

		if(voucher_terpilih[i].tipe == 'Persen') {
			nominal_potongan = nominal * voucher_terpilih[i].diskon_persen / 100;

			if(nominal_potongan > voucher_terpilih[i].max_diskon){
				nominal_potongan = voucher_terpilih[i].max_diskon;
			}
		} else if(voucher_terpilih[i].tipe == 'Nominal') {
			nominal_potongan = voucher_terpilih[i].nominal_diskon;
		}

		$('#nominal_potongan').text(moneyFormat.to(parseInt(nominal_potongan)));
	}
	nominal -= nominal_potongan;

	nominal -= gen_potongan_member();

	$('.total_cart').text(moneyFormat.to(nominal));
	$('.total_potongan_reveral').text(moneyFormat.to(total_potongan));
}

function gen_view(id) {
	//masukkan ke cookie
	// $.cookie('Impressions-cart', JSON.stringify(cart_produk), { path: '/' });
}

function show_cart_list(){
	$('#modal-cart').modal('show');
}

function init_list_cart(){
	$('#list_cart').empty();

	$('#btn-checkout').hide();

	// proses list produk / paket produk
	if(cart_produk.length) {
		$('#list_cart').append('<h4>Produk</h4>');
	}

	for (var i in cart_produk) {
		console.log(i);
		var list = $('#template_list_cart').clone();

		var new_html = list[0].innerHTML;
		//id_item dan tipe-item secara global (regex?) akan di replace, lainnya single replace.
		new_html = new_html.replace(/{{id_item}}/g, cart_produk[i].id);
		new_html = new_html.replace('{{nama_item}}', cart_produk[i].nama);
		new_html = new_html.replace('{{stok}}', cart_produk[i].stok);
		new_html = new_html.replace(/{{tipe-item}}/g, 'produk');
		new_html = new_html.replace('{{harga}}', moneyFormat.to(parseInt(cart_produk[i].harga)));
		if(cart_produk[i].promo){
			new_html = new_html.replace('{{promo}}', cart_produk[i].promo);
		}
		
		//dapatkan value img dari var global imgs_treatment
		var img_template = '';
		// var img_template = $('[data-id="'+cart_produk[i].id+'"][tipe-item="produk"]').find('img_item').html();
		var id_master_produk = cart_produk[i].id_mp;

		if(imgs_produk[id_master_produk].length){
			//ambil img pertama saja
			img_template = imgs_produk[id_master_produk][0]['path'];
		}

		new_html = new_html.replace('{{image}}', img_template);

		new_html = new_html.replace('{{jml}}', cart_produk[i].jml);

		var nominal = 0;
		nominal += cart_produk[i].jml*cart_produk[i].harga;

		new_html = new_html.replace('{{total}}', moneyFormat.to(nominal));

		//jika qty = stok -> disable btn tambah
		if(cart_produk[i].jml >= cart_produk[i].stok){
			new_html = new_html.replace('{{disabled}}', 'disabled');
		}

		// console.log(cart_produk[i].nama);
		// list.replace('{{nama_item}}', cart_produk[i].nama);
		// console.log(list);
		$('#list_cart').append(new_html);

		$('#btn-checkout').show();
	}
}

$("#modal-cart").on('show.bs.modal', function(){
	init_list_cart();
	// var w = $("#list_cart img").width();
	// console.log(w);
	// $("#list_cart img").css({ 'height': w + "px" });
});
$("#modal-cart").on('shown.bs.modal', function(){
	// var w = $("#list_cart img").width();
	// $("#list_cart img").css({ 'height': w + "px" });
});

function pilih_voucher(id_voucher, mode = 'tambah') {
	var response_ajax = [];
	// mode -> tambah/batal
	// variable yg dibutuhkan harus di set sebelum script
	$.ajax({
		url : 'checkout/pilih_voucher',
		type: "POST",
		data: {
			id_voucher:id_voucher,
			mode:mode,
		},
		dataType: "JSON",
		success: function(data)
		{
			response_ajax = data;
			if(data.response){
				gen_voucher(data.voucher);
				voucher_terpilih = data.voucher;

				$('#modal-voucher').modal('hide');
			}else{
				alert('Terjadi Kesalahan.');
			}

			hit_total();
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			alert('Terjadi Kesalahan.');
		}
	}).done(function() {
	  return response_ajax;
	});
}

function gen_voucher(data){
	// hitung nominal pesanan (untuk menghitung minimal_beli)
    var nominal_beli = 0;

    for (var i in cart_produk) {
      nominal_beli += cart_produk[i].jml*cart_produk[i].harga;
    }

	var el = '';

	var nominal_potongan = 0;

	for (var i in data) {

		var teks = '';
		var keterangan = '';
		// tipe voucher -> Nominal/Persen
        // jika persen -> yg d pakai colmn diskon_persen & max_diskon
        // jika nominal -> yg d pakai colmn nominal_diskon

		if(data[i].tipe == 'Persen') {
			nominal_potongan = nominal_beli * data[i].diskon_persen / 100;

			if(nominal_potongan > data[i].max_diskon){
				nominal_potongan = data[i].max_diskon;
			}

			teks = 'Voucher : <b>'+data[i].nama+'</b>';

			keterangan = '* Diskon ' + data[i].diskon_persen + ' % | max ' + moneyFormat.to(parseInt(data[i].max_diskon)) + 
			' | Min belanja : ' + moneyFormat.to(parseInt(data[i].minimal_beli));

		} else if(data[i].tipe == 'Nominal') {
			nominal_potongan = data[i].nominal_diskon;

			teks = 'Voucher : <b>'+data[i].nama+'</b>';
			keterangan = '* Min belanja : ' + moneyFormat.to(parseInt(data[i].minimal_beli));
		}

		el = 	'<div  class="card border-main mb-3">'+
				'<div class="card-body" style="padding-left: 5px; padding-right: 5px;">'+
              		'<div>'+teks+' <b class="pull-right">'+
              		'- Rp. <span id="nominal_potongan">'+moneyFormat.to(parseInt(nominal_potongan))+'</span></b><br>'+
            	'<small>'+ keterangan +'</small></div></div></div>';
	}
	$('#voucher_terpilih').html(el);
}

function gen_potongan_member(){
	// hitung nominal pesanan (untuk menghitung minimal_beli)
    var nominal_beli = 0;

    for (var i in cart_produk) {
      nominal_beli += cart_produk[i].jml*cart_produk[i].harga;
    }

	var el = '';

	var nominal_potongan = 0;

	for (var i in potongan_member) {
		console.log(potongan_member[i]);

		var teks = '';
		var keterangan = '';

		nominal_potongan = nominal_beli * parseFloat(potongan_member[i].value) / 100;

		if(nominal_potongan > parseInt(potongan_member[i].max_diskon) && parseInt(potongan_member[i].max_diskon) > 0){
			nominal_potongan = parseInt(potongan_member[i].max_diskon);
		}

		teks = 'Potongan Member <b>'+potongan_member[i].nama+'</b>';

		keterangan = '* Diskon ' + potongan_member[i].value + ' % | max ' + moneyFormat.to(parseInt(potongan_member[i].max_diskon));


		el = 	'<div  class="card border-main mb-3">'+
			'<div class="card-body" style="padding-left: 5px; padding-right: 5px;">'+
          		'<div>'+teks+' <b class="pull-right">'+
          		'- Rp. <span id="nominal_potongan">'+moneyFormat.to(parseInt(nominal_potongan))+'</span></b><br>'+
        	'<small>'+ keterangan +'</small></div></div></div>';

	}

	$('#potongan_member').html(el);

	return nominal_potongan;
}

