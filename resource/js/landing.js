function kirim_email(){
  $('#btn-mail').addClass('disabled');
  $('#btn-mail').html('Memproses');
  event.preventDefault();
  var data = $('#kirim_email').serializeArray();
  $.ajax({
    url : "<?php echo site_url('landing/send_email')?>",
    data : data,
    type: "Post",
    success: function(result)
    {
      $('#btn-mail').removeClass('disabled');
      $('#btn-mail').html('Kirim Email');
      if(result == 'sukses'){
        alert('Email Terkirim, Terimakasih atas partisipasi anda.');
      }else{
        alert('Terjadi kesalahan, coba beberapa saat lagi.');
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      $('#btn-mail').removeClass('disabled');
      $('#btn-mail').html('Kirim Email');
      alert('Terjadi kesalahan, coba beberapa saat lagi.');
    }
  });
}