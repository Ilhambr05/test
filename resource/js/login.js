$('#modal_login').on('shown.bs.modal', function() {
  		$(this).find('[autofocus]').focus();
  	});

  	function login(){
  		$('#btn_login').addClass('disabled');
  		$('#btn_login').html('Memproses');
  		event.preventDefault();
  		var data = $('#form_login').serializeArray();
  		$.ajax({
  			url : "<?php echo site_url('landing/login')?>",
  			data : data,
  			type: "Post",
  			success: function(result)
  			{
  				$('#btn_login').removeClass('disabled');
  				$('#btn_login').html('Login');
  				alert(result.message);
  				if(result.response){
  					window.location.reload();
  				}
  			},
  			error: function (jqXHR, textStatus, errorThrown)
  			{
  				$('#btn_login').removeClass('disabled');
  				$('#btn_login').html('Login');
  				alert('Terjadi Error');
  			}
  		});
  	}

  	function logout(){
  		if(confirm('Logout')){
  			window.location.href = "<?php echo site_url('landing/logout');?>";
  		}
  	}