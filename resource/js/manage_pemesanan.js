$(function () {
	table = $('#example1').DataTable({
		// Load data for the table's content from an Ajax source
		"processing": true, //Feature control the processing indicator.
		"serverSide": true, //Feature control DataTables' server-side processing mode.
		'responsive': true,
		"ajax": {
			"url": "manage_pemesanan/ajax_list/0/0",
			"type": "POST"
		},
		"autoWidth": true,
		createdRow: function( row, data, dataIndex ) {
			// jika perbaruan transaksi belum dibaca (status_baca = data[7])
			if ( data[7] == '0' ) {
				$(row).css( 'color', 'red' );
			}
		},
		"order": [], //Initial no order.
	});
});

function show_data(){
	table.ajax.url("manage_pemesanan/ajax_list/"+startDate+"/"+endDate).load();
}
