var path = require('path');
var appDir = path.dirname(require.main.filename);
var fs = require( 'fs' );
var app = require('express')();
var https        = require('https');
var path = require("path");
var server = https.createServer({
	key: fs.readFileSync(path.resolve(__dirname, "/ssl.key")),
	cert: fs.readFileSync(path.resolve(__dirname, "/ssl.cert")),
	ca: fs.readFileSync(path.resolve(__dirname, "/ssl.cert")),
	requestCert: false,
	rejectUnauthorized: false
},app);
server.listen(3000);

var io = require('socket.io').listen(server);
// server.listen(port, function () {
//  console.log('Server listening at port %d', port);
// });


io.on('connection', function (socket) {

	socket.on( 'pesanan_masuk', function( data ) {
		// console.log("pesanan_masuk server");
		io.sockets.emit( 'pesanan_masuk', data);
	});

	socket.on( 'pesanan_dibayarkan', function( data ) {
		// console.log("pesanan_dibayar server");
		io.sockets.emit( 'pesanan_dibayarkan', data);
	});

	socket.on( 'perubahan_status', function( data ) {
		// console.log("ada perubahan statusr");
		io.sockets.emit( 'perubahan_status', data);
	});

	socket.on( 'perkembangan_pemesanan', function( data ) {
		// console.log(data);
		io.sockets.emit( 'perkembangan_pemesanan', data);
	});

	socket.on( 'status_pemesanan', function( data ) {
		// console.log(data);
		io.sockets.emit( 'status_pemesanan', data);
	});

});
