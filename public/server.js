var socket  = require( 'socket.io' );
var express = require('express');
var app     = express();
var server  = require('http').createServer(app);
var io      = socket.listen( server );
var port    = process.env.PORT || 3000;


server.listen(port, function () {
 console.log('Server listening at port %d', port);
});


io.on('connection', function (socket) {

  socket.on( 'pesanan_masuk', function( data ) {
    // console.log("pesanan_masuk server");
    io.sockets.emit( 'pesanan_masuk', data);
  });

  socket.on( 'pesanan_dibayarkan', function( data ) {
    // console.log("pesanan_dibayar server");
    io.sockets.emit( 'pesanan_dibayarkan', data);
  });

  socket.on( 'perubahan_status', function( data ) {
  	// console.log("ada perubahan statusr");
	io.sockets.emit( 'perubahan_status', data);
  });

  socket.on( 'perkembangan_pemesanan', function( data ) {
  	// console.log(data);
	  io.sockets.emit( 'perkembangan_pemesanan', data);
  });

  socket.on( 'status_pemesanan', function( data ) {
  	// console.log(data);
  	io.sockets.emit( 'status_pemesanan', data);
  });

});
