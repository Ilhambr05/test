var password = document.getElementById("password")
, confirm_password = document.getElementById("retypePassword");

function validatePassword(){
  if(password.value == ''){
    password.setCustomValidity("Harap Diisi");
  }
  else if(confirm_password.value == ''){
    confirm_password.setCustomValidity("Harap Diisi");
  }
  else if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Tidak cocok");
  } else {
    confirm_password.setCustomValidity('');
  }
}

// password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

function ganti_pass(){
  $('.btn').addClass('disabled');
  $('#btn_pass').html('Memproses');
  event.preventDefault();
  var data = $('#form_password').serializeArray();
  $.ajax({
    url : "profile/change_pass",
    data : data,
    type: "Post",
    success: function(result)
    {
      $('.btn').removeClass('disabled');
      $('#btn_pass').html('Ubah Password');
      alert(result.message);
      if(result.response){
        window.location.href = 'login';
      }else{
        $('[name="pass_lama"]').focus();
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      $('.btn').removeClass('disabled');
      $('#btn_pass').html('Ubah Password');
      alert('Terjadi Error');
    }
  });
}