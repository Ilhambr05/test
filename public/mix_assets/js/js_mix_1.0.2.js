$(window).load(function() {
    $('.title').css({
        'top': 90 + 'px',
        'opacity': 1
    });
    $('.text').css({
        'opacity': 1
    });
    $('.more').css({
        'opacity': 1,
        'bottom': 90 + 'px'
    });
});
$('#header-carousel').on('slid.bs.carousel', function() {
    $('.title').css({
        'top': 90 + 'px',
        'opacity': 1
    });
    $('.text').css({
        'opacity': 1
    });
    $('.more').css({
        'opacity': 1,
        'bottom': 90 + 'px'
    });
});
$('#header-carousel').on('slide.bs.carousel', function() {
    $('.title').css({
        'top': 0 + 'px',
        'opacity': 0
    });
    $('.text').css({
        'opacity': 0
    });
    $('.more').css({
        'opacity': 0,
        'bottom': 0 + 'px'
    });
});

function carouselFix() {
    $(".carousel.slide").carousel("pause");
    $('.carousel.slide .item').removeClass('active');
    $('.carousel.slide').find('.item:first').addClass('active');
}
$(document).ready(function() {
    carouselFix();
});
/*var $item = $('.carousel .item');
var $wHeight = $(window).height()-120;
$item.eq(0).addClass('active');
$item.height($wHeight);
$item.addClass('full-screen');
$('.carousel img').each(function() {
    var $src = $(this).attr('src');
    var $color = $(this).attr('data-color');
    $(this).parent().css({
        'background-image': 'url(' + $src + ')',
        'background-color': $color
    });
    $(this).remove();
});
$(window).on('resize', function() {
    $wHeight = $(window).height()-120;
    $item.height($wHeight);
});*/
$('.carousel').carousel({
    interval: 6000,
    pause: "false"
});

$(function(){
    /*$('a').attr('href','#');*/

    $('.icofont-search-alt-1').click(function(e){
        e.preventDefault();
        $('.top-search').fadeIn();
        $('html, body').animate({
            scrollTop: $('.top-search').offset().top-80
        }, 1000);
    });

    $('.treatment_click').mouseenter(function(e){
        $.cookie('treatment', $(this).attr('idp'));
    });

    var isMobile = false;
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
        $('.button_wa_banner').click(function(){
            if(isMobile){
                top.location='https://api.whatsapp.com/send?phone=628111109066';
            }else{
                $('.modal-dialog').addClass('small');
                $('#modal').modal('toggle');
                $('#modal #myModalLabel').html('Alert');
                $('#modal .body').html('Click This Button On Your Phone');
            }
        });        

        /*$('li a#contact').append('<div class="scroll"></div>');*/
        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();
            /*$('.scroll').html(scroll);*/
            if(scroll>=80){
                $('.logoimg').addClass('scroll');
            }else{
                $('.logoimg').removeClass('scroll');
            }
        });

        $('.button_contact').click(function(){
            $('.form_contact :input').each(function () {
                if (!$(this).val()) {
                    $(this).addClass('errorfill');
                } else {
                    $(this).removeClass('errorfill');
                }
            });

            if($('.errorfill').length){
                throw'stop';
            }

            var parameter={
                'ref':'contact',
                'name':$('[name="name"]').val(),
                'email':$('[name="email"]').val(),
                'message':$('[name="message"]').val(),
            }

            $(this).attr('disabled','disabled').html('saving...');
            $.post('<?php echo site_url(); ?>action.php',parameter,function(data){
                alert('success');
                top.location=window.location;
            });
        });

    });
$('#modal-voucher').on('show.bs.modal', function (e) {
  gen_list_voucher();
})

function get_id_voucher_terpilih(){
  id = '';
  for (var i in voucher_terpilih) {
    id = voucher_terpilih[i].id_voucher;
  }

  return id;
}

function gen_list_voucher() {
  // hitung nominal pesanan (untuk menghitung minimal_beli)
  var nominal_beli = 0;

  for (var i in cart_produk) {
    nominal_beli += cart_produk[i].jml*cart_produk[i].harga;
  }

  for (var i in cart_treatment) {
    nominal_beli += cart_treatment[i].jml*cart_treatment[i].harga;
  }

  $('#list_voucher').empty();

  $.ajax({
    url : 'checkout/get_list_voucher_member',
    type: "POST",
    dataType: "JSON",
    success: function(data)
    {
      for (var i in data) {
        // console.log(i);
        var list = $('#template_list_voucher').clone();

        var new_html = list[0].innerHTML;
        //id_item dan tipe-item secara global (regex?) akan di replace, lainnya single replace.
        new_html = new_html.replace(/{{id_voucher}}/g, data[i].id_voucher);
        new_html = new_html.replace('{{nama}}', data[i].nama);
        new_html = new_html.replace('{{kuota}}', data[i].kuota_sisa);
        new_html = new_html.replace(/{{end}}/g, data[i].end);

        if(data[i].tipe == 'Persen') {
          new_html = new_html.replace('{{potongan}}', "Diskon "+data[i].diskon_persen+" % | Max Rp. "+moneyFormat.to(parseInt(data[i].max_diskon)));
        } else if(data[i].tipe == 'Nominal') {
          new_html = new_html.replace('{{potongan}}', "Potongan Rp. "+moneyFormat.to(parseInt(data[i].nominal_diskon)));
        }

        new_html = new_html.replace('{{min_beli}}', "* Minimal Pembelian Rp. "+moneyFormat.to(parseInt(data[i].minimal_beli)));

        // disable tombol jika tidak memenuhi syarat minimal pembelian
        if( nominal_beli < data[i].minimal_beli ){
          // new_html = new_html.replace('disabled', 'disabled');
          new_html = new_html.replace('{{btn-class}}', 'text-black');
          new_html = new_html.replace('{{mode}}', '----');
          new_html = new_html.replace('{{text-btn}}', 'Tidak Dapat Digunakan');
        } else {
          new_html = new_html.replace('disabled', '');
        }

        // jika id voucher sama dg yg dipilih, berikan opsi batalkan voucher
        // mode -> tambah/batal
        if(data[i].id_voucher == get_id_voucher_terpilih()) {
          new_html = new_html.replace('{{btn-class}}', 'btn-danger');
          new_html = new_html.replace('{{mode}}', 'batal');
          new_html = new_html.replace('{{text-btn}}', 'Batalkan Voucher');
        } else {
          new_html = new_html.replace('{{btn-class}}', 'btn-custom');
          new_html = new_html.replace('{{mode}}', 'tambah');
          new_html = new_html.replace('{{text-btn}}', 'Gunakan Voucher');
        }

        $('#list_voucher').append(new_html);
      }

      //jika tak ada voucher tersedia
      if(!data.length){
        $('#list_voucher').append('<h4 class="text-center">Tidak Ada Voucher</h4>');
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Terjadi Error');
      // location.reload();
    }
  });
}

// pemilihan alamat ---------------------------------------

//pilih prov
$('.pilih_prov').select2({
  // minimumInputLength: 1,
  placeholder: 'Pilih Provinsi',
  dropdownParent: $("#modal-tambah-alamat"),
  ajax: {
    url: 'checkout/cari_prov',
    type: "post",
    delay: 200,
    data: function (params) {
      var query = {
        key: params.term,
      }
      return query;
    },
    processResults: function (data) {
      data.push({id:0, text:'Tidak Tersedia di list'});
      return {
        results: data
      };
    },
  },
}).on('select2:select', function (e) {
  $('.pilih_kec, .pilih_deskel').prop('disabled', true).val("").trigger("change");
  $('.pilih_kabkot').prop('disabled', false);
});


//pilih kabkot
$('.pilih_kabkot').select2({
  // minimumInputLength: 1,
  placeholder: 'Pilih Kabupaten / Kota',
  dropdownParent: $("#modal-tambah-alamat"),
  ajax: {
    url: 'checkout/cari_kabkot',
    type: "post",
    delay: 200,
    data: function (params) {
      var query = {
        key: params.term,
        id_prov: $('.pilih_prov').val()
      }
      return query;
    },
    processResults: function (data) {
      data.push({id:0, text:'Tidak Tersedia di list'});
      return {
        results: data
      };
    },
  },
}).on('select2:select', function (e) {
  $('.pilih_deskel').prop('disabled', true).val("").trigger("change");
  $('.pilih_kec').prop('disabled', false);
});

//pilih kec
$('.pilih_kec').select2({
  // minimumInputLength: 1,
  placeholder: 'Pilih Kecamatan',
  dropdownParent: $("#modal-tambah-alamat"),
  ajax: {
    url: 'checkout/cari_kec',
    type: "post",
    delay: 200,
    data: function (params) {
      var query = {
        key: params.term,
        id_kabkot: $('.pilih_kabkot').val()
      }
      return query;
    },
    processResults: function (data) {
      data.push({id:0, text:'Tidak Tersedia di list'});
      return {
        results: data
      };
    },
  },
}).on('select2:select', function (e) {
  $('.pilih_deskel').prop('disabled', false);
});

// --------------------------------------- pemilihan alamat 

function proses_alamat_baru(){
  $('.btn').addClass('disabled');
  event.preventDefault();
  var data = $('#form_alamat_baru').serializeArray();
  $.ajax({
    url : 'checkout/proses_alamat_baru',
    data : data,
    type: "Post",
    success: function(result)
    {
      $('.btn').removeClass('disabled');
      if(result.response){
        alert('Alamat ditambahkan.');
        $('#modal-tambah-alamat').modal('hide');
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      $('.btn').removeClass('disabled');
      alert('Terjadi Error');
    }
  });
}

$('#modal-pilih-alamat').on('show.bs.modal', function (e) {
  gen_list_alamat();
})

function get_id_alamat_terpilih(){
  id = '';
  for (var i in alamat_terpilih) {
    id = alamat_terpilih[i].id_pengiriman_member;
  }

  return id;
}

function gen_list_alamat() {
  $('#list_alamat').empty();

  $.ajax({
    url : 'checkout/get_list_alamat_member',
    type: "POST",
    dataType: "JSON",
    success: function(data)
    {
      for (var i in data) {
        //simpan list alamat ke variable untuk nanti digunakan setelah jalankan fungsi pilih_alamat
        list_alamat = data;
        // console.log(i);
        var list = $('#template_list_alamat').clone();

        var new_html = list[0].innerHTML;
        //id_item dan tipe-item secara global (regex?) akan di replace, lainnya single replace.
        new_html = new_html.replace(/{{id_pengiriman_member}}/g, data[i].id_pengiriman_member);
        new_html = new_html.replace('{{nama_profile}}', data[i].nama_profile);
        new_html = new_html.replace('{{nama_penerima}}', data[i].nama_penerima);
        new_html = new_html.replace('{{alamat}}', data[i].alamat);
        new_html = new_html.replace(/{{kode_pos}}/g, data[i].kode_pos);
        new_html = new_html.replace('{{no_tlpn}}', data[i].no_tlpn);

        $('#list_alamat').append(new_html);
      }

      //jika tak ada Alamat tersimpan
      if(!data.length){
        $('#list_alamat').append('<h4 class="text-center">Belum Ada Alamat Tersimpan, Harap Tambah Dahulu</h4>');
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Terjadi Error');
      // location.reload();
    }
  });
}

function pilih_alamat(id_pengiriman_member){
  for (var i in list_alamat) {
    //cari id pengiriman_member di variable
    if( list_alamat[i].id_pengiriman_member == id_pengiriman_member ){

      gen_alamat(list_alamat[i]);
      break;

    }
  }

  $('#modal-pilih-alamat').modal('hide');
}

function gen_alamat(data){
  $(' #id_pengiriman_member ').val( data.id_pengiriman_member );

  var el = '';

  for (var i in data) {
    el =  '<div class="card border-main">'+
    '<div class="card-header text-center">'+
    '<b>'+data.nama_profile+'</b>'+
    '</div>'+
    '<div class="card-body">'+
    '<p><b>Alamat &nbsp &nbsp &nbsp : </b>'+ data.alamat +' | '+ data.kode_pos +'</p>'+
    '<p><b>Penerima &nbsp: </b>'+ data.nama_penerima +' | '+ data.no_tlpn +'</p>'+
    '</div>'+
    '</div>';

    //hilangkan border alert pada btn selector alamat
    $("#btn_pemilihan_alamat").removeClass('border-alert-bottom');
  }

  $('#alamat_terpilih').html(el);
}

function proses_pemesanan()
{
  // jika tidak ada produk terpilih dan belum memilih alamat
  if(! $(' #id_pengiriman_member ').val() && cart_member.produk.length) {

    alert('Harap Pilih Alamat Pengiriman');

    $("#btn_pemilihan_alamat").addClass('border-alert-bottom');

    $('html, body').animate({
      scrollTop: $("#btn_pemilihan_alamat").offset().top - $(window).height()/2 + 25
    }, 0);

  } else {

    $('#btn_pesan').attr('disabled', true);
    $('#btn_pesan').html('<i class="material-icons" style="top: -2px !important;">cloud_upload</i> &nbsp Memproses, Mohon Tunggu ...');

    // kirimkan total hitung client-side untuk dibandingkan di server-side
    var total_cart_sebelum_potongan = moneyFormat.from( $('.total_cart_sebelum_potongan').text() );
    var total_cart = moneyFormat.from( $('.total_cart').text() );

    var data_pesan = {
      total_cart_sebelum_potongan : total_cart_sebelum_potongan,
      grand_total_cart : total_cart,
      id_pengiriman_member : $(' #id_pengiriman_member ').val(),
    };

    $.ajax({
      url : 'checkout/proses_pemesanan',
      type: "POST",
      dataType: "JSON",
      data:data_pesan,
      success: function(data)
      {
        // alert(data);
        if(data.response){
          // lanjutkan_proses_pemesanan(data.snap_token);
          window.location.replace('transaksi/list_cart/' + data.id_transaksi);
        } else {
          if(data.error == 'total missmatch') {
            alert('Terjadi Kesalahan');

            // location.reload();
          }
          if(data.error == 'midtrans error') {
            alert('Silahkan Coba Beberapa Saat Lagi.');
          }
        }
        $('#btn_pesan').attr('disabled', false);
        $('#btn_pesan').html('<i class="material-icons" style="top: -2px !important;">done</i> &nbsp Proses Pemesanan');
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Terjadi Error');
        $('#btn_pesan').attr('disabled', false);
        $('#btn_pesan').html('<i class="material-icons" style="top: -2px !important;">done</i> &nbsp Proses Pemesanan');
      }
    });
    
  }
}
// beberapa value untuk kegunaan script ini harus di set sebelumnya di file berbeda.

function set_rasio_img(){
	// var w = $(".img_item").width();
	// $(".img_item, .detail-btn, .ket_jml").css({ 'height': w + "px" });

	// var w = $("#list_cart img").width();
	// $("#list_cart img").css({ 'height': w + "px" });
}

var waitForFinalEvent = (function () { 
	//delay sampai event selesai execute
	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) {
			uniqueId = "Don't call this twice without a uniqueId";
		}
		if (timers[uniqueId]) {
			clearTimeout (timers[uniqueId]);
		}
		timers[uniqueId] = setTimeout(callback, ms);
	};
})();

$(window).resize(function () { 
	// panggil ketika screen size berubah
	waitForFinalEvent(function(){
		set_rasio_img();
	}, 500, "win resize selesai");
});

var moneyFormat = wNumb({
	mark: ',',
	decimals: 0,
	thousand: '.',
	prefix: '',
	suffix: ''
});

$('a[data-toggle="pill"]').on('hide.bs.tab', function (e) {
	//scroll ke atas jika pill di tekan.
	$('html, body').animate({
        scrollTop: $('.section').offset().top - $('#sticky_navigation').outerHeight()
    }, 0);
	// console.log(e.target); // newly activated tab
	// console.log(e.relatedTarget); // previous active tab
})

var cart_produk = get_data_cart('produk');

$( document ).ready(function() {
	//$.removeCookie('Impressions-cart');

	// panggil di page jika document sudah siap
	// *tak masalah document.ready dipanggil lebih dari sekali di satu page
	init_item_selected();
});

function get_data_cart(tipe){
	if(tipe == 'produk'){
		if(upload_ajax){
			// value cart d ambil dari pengaturan script sebelumnya
			var data_cart = cart_member.produk;

			return data_cart;
		}else{
			if($.cookie('Impressions-cart')){
				return JSON.parse($.cookie('Impressions-cart'));
			}
			else{
				return [];
			}
		}
	}

	else if(tipe == 'treatment'){
		if(upload_ajax){
			// value cart d ambil dari pengaturan script sebelumnya
			var data_cart = cart_member.treatment;

			return data_cart;
		}else{
			if($.cookie('Impressions-cart-treatment')){
				return JSON.parse($.cookie('Impressions-cart-treatment'));
			}
			else{
				return [];
			}
		}
	}
}

function init_item_selected(){
	//membuat tampilan item yang sudah masuk di cart

	var nominal = 0;

	for (var i in cart_produk) {
		var el = $('[data-id="'+cart_produk[i].id+'"][tipe-item="produk"]');
		el.find('.ket_jml').show();
		if (cart_produk[i].jml > 1) {
			el.find('.icon_del').text('exposure_minus_1');
		}
		el.find('.val_jml').text(cart_produk[i].jml);
		$('#modal-cart').find('#total_item_'+cart_produk[i].id).text(cart_produk[i].jml);

		el.find('.btn_beli').hide();
		el.find('.jml_beli').show();

		var harga = el.find('harga').text();
		harga = parseInt(harga.replace(/\./g,''));

		if(cart_produk[i].jml >= cart_produk[i].stok){
			el.find('.jml_beli .btn-success').prop('disabled', true);
		}else{
			el.find('.jml_beli .btn-success').prop('disabled', false);
		}

		nominal += cart_produk[i].jml*harga;
	}

	$('#jml_cart').text(cart_produk.length);

	hit_total();
}

function detail(id, tipe){
	var selector = $('[data-id="'+id+'"][tipe-item="'+tipe+'"]');
	var bg = selector.find('img_item').text();
	bg = bg.replace(/ /g, '%20');

	$('#modal-detail nama').text(selector.find('nama').text());
	$('#modal-detail harga').text(selector.find('harga').text());
	$('#modal-detail satuan').text(selector.find('satuan').text());
	$('#modal-detail promo').text(selector.find('promo').text());
	$('#modal-detail deskripsi').html(selector.find('deskripsi').html());
	$('#modal-detail .img_item').css('background-image', "url('"+selector.find('img_item').text()+"')");

	//tipe produk/treatment
	// var tipe = selector.attr('tipe-item');

	if(tipe == 'produk'){
		//var imgs_produk harus disediakan sebelum script ini
		var id_master_produk = selector.attr('id-master-produk');

		var el = '';
		$.each(imgs_produk[id_master_produk], function( index, value ) {
			if(index > 0){
				// var url = 'url("'+value.path+'")';
				el += 
					'<div class="col-xs-12 col-sm-12 col-md-4">'+
						'<img class="rounded img-raised w-100" src="'+value.path+'"></img>'+
					'</div>';
			}
		});
		console.log(el);
		$('#img_detail').html(el);
	}

	else if(tipe == 'treatment'){
		//var imgs_produk harus disediakan sebelum script ini
		var id_master_treatment = selector.attr('id-master-treatment');

		var el = '';
		$.each(imgs_treatment[id_master_treatment], function( index, value ) {
			// var url = 'url("'+value.path+'")';
			if(index > 0) {
				el += 
					'<div class="col-xs-12 col-sm-12 col-md-4">'+
						'<img class="rounded img-raised w-100" src="'+value.path+'"></img>'+
					'</div>';
			}
		});
		console.log(el);
		$('#img_detail').html(el);
	}

	$('#modal-detail').modal('show');
}

function beli(id_item,btn,tipe){
	$(btn).parent().hide();
	$(btn).parent().siblings('.jml_beli').show();

	add_cart(id_item, tipe);
}

function add_cart(id_item, tipe)
{
	if(upload_ajax)
	{
		// mode add/min/remove
		$.ajax({
	    	url : 'shop/manage_cart',
	    	type: "POST",
	    	data: {
	    		id_item: id_item,
	    		mode: 'add',
	    		tipe: tipe,
	    	},
	    	dataType: "JSON",
	    	success: function(data)
	    	{
	    		if(data.response){
					atur_view_add(id_item, tipe);
	    		}else{
	    			alert('Terjadi Kesalahan.');
	        		// location.reload();
	    		}
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	        	alert('Terjadi Kesalahan server.');
	        	// location.reload();
	        }
	    });
	}
	else
	{
		atur_view_add(id_item, tipe);
	}
}

function atur_view_add(id_item, tipe)
{
	var exist = 0;
	var jml_arr = 1;

	if(tipe == 'produk') {
		var el = $('[data-id="'+id_item+'"][tipe-item="produk"]');

		console.log('selector utama');
		console.log(el);

		el.find('.ket_jml').show();

		for (var i in cart_produk) {
			if (cart_produk[i].id == id_item) {

				jml_arr = parseInt(cart_produk[i].jml) + 1;
				cart_produk[i].jml = jml_arr;
				
				exist = 1;
				el.find('.icon_del').text('exposure_minus_1');

				var nominal = 0;
				nominal += cart_produk[i].jml*cart_produk[i].harga;

				el.find('#total_harga_'+id_item).text(moneyFormat.to(nominal));

				if(jml_arr >= cart_produk[i].stok) {
					// disable button untuk nambah di cart produk
					el.find('.jml_beli .btn-success').prop('disabled', true);

					// disable button untuk nambah di cart produk
					el.find('.handler_jumlah_item_cart .btn-success').prop('disabled', true);
				}

				break; 
				//Stop loop
			}
		}
		
		el.find('.icon_del').text('exposure_minus_1');

		el.find('.val_jml').text(jml_arr);
		el.find('#total_item_'+id_item).text(jml_arr);

		if(!exist) {
			var nama = el.find('nama').text();
			// var image = el.find('img').prop('src');
			var image = el.find('img_item').text();
			var harga = el.find('harga').text();
			var stok = el.find('stok').text();
			var tipe_produk = el.attr('tipe-produk');
			var id_mp = el.attr('id-master-produk');
			harga = parseInt(harga.replace(/\./g,''));

			var obj = {id:id_item, id_mp:id_mp, jml:1, stok:stok, nama:nama, tipe:tipe_produk, harga:harga};

			cart_produk.push(obj);
			console.log('tambah item cart lokal - cookie');
			//tambah obj
		}

		console.log('jml_arr ' + jml_arr);

	} 

	// console.log(jml_arr);

	proses_lanjutan();
}

function cek_status_voucher(id_item, tipe, remove)
{
	var voucher_bisa_digunakan = true;
	// CEK STATUS VOUCHER JIKA QTY ITEM DIKURANGI

	var nominal_cart = 0;

	for (var i in cart_produk) {
		nominal_cart += cart_produk[i].jml*cart_produk[i].harga;
	}

	var minimal_beli = 0;
	var id_voucher = 0;
	for ( var i in voucher_terpilih ) {
		minimal_beli = voucher_terpilih[i].minimal_beli;
		id_voucher = voucher_terpilih[i].id_voucher;
	}

	var value_pengurangan = 0;

	if( tipe == 'produk' ) {
		for (var i in cart_produk) {
			if (cart_produk[i].id == id_item) {

				if(remove){

					value_pengurangan = parseInt(cart_produk[i].jml) * parseInt(cart_produk[i].harga);

				} else {

					value_pengurangan = parseInt(cart_produk[i].harga);

				}
				
				break; 
				//Stop loop
			}
		}
	}
		
	// jika value nominal_cart nantinya kurang dari minimal_beli pada voucher
	var cek = parseInt(nominal_cart) - parseInt(value_pengurangan);
	console.log('nominal ' + cek);
	console.log(minimal_beli);
	if( cek < minimal_beli ){
		var lanjut = confirm( 'Voucher Akan Menjadi Tidak Valid, Lanjutkan ?' );

		if( lanjut ){
			// pilih_voucher(id_voucher, 'batal');
			voucher_bisa_digunakan = false;
		} else {
			voucher_bisa_digunakan = true;
		}
	}

	// console.log(voucher_bisa_digunakan);

	return voucher_bisa_digunakan;
}

function rm_cart(id_item,btn,remove=0, tipe){

	var voucher_bisa_digunakan = cek_status_voucher(id_item, tipe, remove);

	if( upload_ajax )
	{
		// rm_ajax(id_item, btn, remove);
		var mode_rm = 'min';

		if(remove){
			mode_rm = 'remove';
		}

		// mode add/min/remove
		$.ajax({
	    	url : 'shop/manage_cart',
	    	type: "POST",
	    	data: {
	    		id_item: id_item,
	    		mode: mode_rm,
	    		tipe: tipe,
	    	},
	    	dataType: "JSON",
	    	success: function(data)
	    	{
	    		if(data.response){
	    			atur_view_rm(id_item, btn, remove, tipe);
	    		}else{
	    			alert('Terjadi Kesalahan.');
	        		// location.reload();
	    		}
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	        	alert('Terjadi Kesalahan server.');
	        	// location.reload();
	        }
	    }).done( function() {
	    	//kalau proses ajax selesai dan voucher tidak bisa digunakan
	    	if(!voucher_bisa_digunakan){
	    		//batalkan voucher
	    		pilih_voucher(0, 'batal');
	    	}
		});
	}
	else
	{
		atur_view_rm(id_item, btn, remove, tipe);
	}
}

function atur_view_rm(id_item, btn, remove, tipe)
{
	var hide = 0;
	var jml_arr = 1;

	// console.log('remove '+remove);
	if( tipe == 'produk' ) {

		var el = $('[data-id="'+id_item+'"][tipe-item="produk"]');
		for (var i in cart_produk) {
			if (cart_produk[i].id == id_item) {
				
				cart_produk[i].jml = jml_arr = parseInt(cart_produk[i].jml) - 1;
				// jml_arr = cart_produk[i].jml;

				var nominal = 0;

				nominal += parseInt(cart_produk[i].jml) * parseInt(cart_produk[i].harga);

				if(remove){
					cart_produk[i].jml = 0; 
					hide = 1;
				}
				if(jml_arr <= 1){
					el.find('.icon_del').text('highlight_off');
				}
				if(jml_arr <= 0){
					//remove arr index
					cart_produk.splice(i, 1);
					hide = 1;
					jml_arr = 0;
					$(btn).closest('.card-cart').remove();
				}

				el.find('#total_harga_'+id_item).text(moneyFormat.to(nominal));
				
				break; 
				//Stop loop
			}
		}		

		el.find('.val_jml').text(jml_arr);
		el.find('#total_item_'+id_item).text(jml_arr);
		
		if(hide){
			el.find('.ket_jml, .jml_beli').hide();
			$(btn).closest('.card-cart').remove();
			el.find('.btn_beli').show();
		}

		if(jml_arr != el.find('stok').text()){
			// un-disable button untuk nambah di list produk
			el.find('.jml_beli .btn-success').prop('disabled', false);

			// un-disable button untuk nambah di cart produk
			el.find('.handler_jumlah_item_cart .btn-success').prop('disabled', false);
		}

	}

	proses_lanjutan();
}

function proses_lanjutan(){
	//masukkan ke cookie (jika belum login)
	if(!upload_ajax)
	{
		$.cookie('Impressions-cart', JSON.stringify(cart_produk), { path: '/' });
	}
	$('#jml_cart').text(cart_produk.length);

	hit_total();

	//jika isi cart kosong
	if(!cart_produk.length){
		$('#modal-cart').modal('hide');
	}
}

function hit_total()
{
	var nominal = 0;
	var total_potongan = 0;
	for (var i in cart_produk) {
		nominal += cart_produk[i].jml*cart_produk[i].harga;
	}

	$('.total_cart_sebelum_potongan').text(moneyFormat.to(nominal));

	var nominal_potongan = 0;
	// hitung potongan
	for ( var i in voucher_terpilih ) {
		
		// tipe voucher -> Nominal/Persen
        // jika persen -> yg d pakai colmn diskon_persen & max_diskon
        // jika nominal -> yg d pakai colmn nominal_diskon

		if(voucher_terpilih[i].tipe == 'Persen') {
			nominal_potongan = nominal * voucher_terpilih[i].diskon_persen / 100;

			if(nominal_potongan > voucher_terpilih[i].max_diskon){
				nominal_potongan = voucher_terpilih[i].max_diskon;
			}
		} else if(voucher_terpilih[i].tipe == 'Nominal') {
			nominal_potongan = voucher_terpilih[i].nominal_diskon;
		}

		$('#nominal_potongan').text(moneyFormat.to(parseInt(nominal_potongan)));
	}
	nominal -= nominal_potongan;

	nominal -= gen_potongan_member();

	$('.total_cart').text(moneyFormat.to(nominal));
	$('.total_potongan_reveral').text(moneyFormat.to(total_potongan));
}

function gen_view(id) {
	//masukkan ke cookie
	// $.cookie('Impressions-cart', JSON.stringify(cart_produk), { path: '/' });
}

function show_cart_list(){
	$('#modal-cart').modal('show');
}

function init_list_cart(){
	$('#list_cart').empty();

	$('#btn-checkout').hide();

	// proses list produk / paket produk
	if(cart_produk.length) {
		$('#list_cart').append('<h4>Produk</h4>');
	}

	for (var i in cart_produk) {
		console.log(i);
		var list = $('#template_list_cart').clone();

		var new_html = list[0].innerHTML;
		//id_item dan tipe-item secara global (regex?) akan di replace, lainnya single replace.
		new_html = new_html.replace(/{{id_item}}/g, cart_produk[i].id);
		new_html = new_html.replace('{{nama_item}}', cart_produk[i].nama);
		new_html = new_html.replace('{{stok}}', cart_produk[i].stok);
		new_html = new_html.replace(/{{tipe-item}}/g, 'produk');
		new_html = new_html.replace('{{harga}}', moneyFormat.to(parseInt(cart_produk[i].harga)));
		if(cart_produk[i].promo){
			new_html = new_html.replace('{{promo}}', cart_produk[i].promo);
		}
		
		//dapatkan value img dari var global imgs_treatment
		var img_template = '';
		// var img_template = $('[data-id="'+cart_produk[i].id+'"][tipe-item="produk"]').find('img_item').html();
		var id_master_produk = cart_produk[i].id_mp;

		if(imgs_produk[id_master_produk].length){
			//ambil img pertama saja
			img_template = imgs_produk[id_master_produk][0]['path'];
		}

		new_html = new_html.replace('{{image}}', img_template);

		new_html = new_html.replace('{{jml}}', cart_produk[i].jml);

		var nominal = 0;
		nominal += cart_produk[i].jml*cart_produk[i].harga;

		new_html = new_html.replace('{{total}}', moneyFormat.to(nominal));

		//jika qty = stok -> disable btn tambah
		if(cart_produk[i].jml >= cart_produk[i].stok){
			new_html = new_html.replace('{{disabled}}', 'disabled');
		}

		// console.log(cart_produk[i].nama);
		// list.replace('{{nama_item}}', cart_produk[i].nama);
		// console.log(list);
		$('#list_cart').append(new_html);

		$('#btn-checkout').show();
	}
}

$("#modal-cart").on('show.bs.modal', function(){
	init_list_cart();
	// var w = $("#list_cart img").width();
	// console.log(w);
	// $("#list_cart img").css({ 'height': w + "px" });
});
$("#modal-cart").on('shown.bs.modal', function(){
	// var w = $("#list_cart img").width();
	// $("#list_cart img").css({ 'height': w + "px" });
});

function pilih_voucher(id_voucher, mode = 'tambah') {
	var response_ajax = [];
	// mode -> tambah/batal
	// variable yg dibutuhkan harus di set sebelum script
	$.ajax({
		url : 'checkout/pilih_voucher',
		type: "POST",
		data: {
			id_voucher:id_voucher,
			mode:mode,
		},
		dataType: "JSON",
		success: function(data)
		{
			response_ajax = data;
			if(data.response){
				gen_voucher(data.voucher);
				voucher_terpilih = data.voucher;

				$('#modal-voucher').modal('hide');
			}else{
				alert('Terjadi Kesalahan.');
			}

			hit_total();
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			alert('Terjadi Kesalahan.');
		}
	}).done(function() {
	  return response_ajax;
	});
}

function gen_voucher(data){
	// hitung nominal pesanan (untuk menghitung minimal_beli)
    var nominal_beli = 0;

    for (var i in cart_produk) {
      nominal_beli += cart_produk[i].jml*cart_produk[i].harga;
    }

	var el = '';

	var nominal_potongan = 0;

	for (var i in data) {

		var teks = '';
		var keterangan = '';
		// tipe voucher -> Nominal/Persen
        // jika persen -> yg d pakai colmn diskon_persen & max_diskon
        // jika nominal -> yg d pakai colmn nominal_diskon

		if(data[i].tipe == 'Persen') {
			nominal_potongan = nominal_beli * data[i].diskon_persen / 100;

			if(nominal_potongan > data[i].max_diskon){
				nominal_potongan = data[i].max_diskon;
			}

			teks = 'Voucher : <b>'+data[i].nama+'</b>';

			keterangan = '* Diskon ' + data[i].diskon_persen + ' % | max ' + moneyFormat.to(parseInt(data[i].max_diskon)) + 
			' | Min belanja : ' + moneyFormat.to(parseInt(data[i].minimal_beli));

		} else if(data[i].tipe == 'Nominal') {
			nominal_potongan = data[i].nominal_diskon;

			teks = 'Voucher : <b>'+data[i].nama+'</b>';
			keterangan = '* Min belanja : ' + moneyFormat.to(parseInt(data[i].minimal_beli));
		}

		el = 	'<div  class="card border-main mb-3">'+
				'<div class="card-body" style="padding-left: 5px; padding-right: 5px;">'+
              		'<div>'+teks+' <b class="pull-right">'+
              		'- Rp. <span id="nominal_potongan">'+moneyFormat.to(parseInt(nominal_potongan))+'</span></b><br>'+
            	'<small>'+ keterangan +'</small></div></div></div>';
	}
	$('#voucher_terpilih').html(el);
}

function gen_potongan_member(){
	// hitung nominal pesanan (untuk menghitung minimal_beli)
    var nominal_beli = 0;

    for (var i in cart_produk) {
      nominal_beli += cart_produk[i].jml*cart_produk[i].harga;
    }

	var el = '';

	var nominal_potongan = 0;

	for (var i in potongan_member) {
		console.log(potongan_member[i]);

		var teks = '';
		var keterangan = '';

		nominal_potongan = nominal_beli * parseFloat(potongan_member[i].value) / 100;

		if(nominal_potongan > parseInt(potongan_member[i].max_diskon) && parseInt(potongan_member[i].max_diskon) > 0){
			nominal_potongan = parseInt(potongan_member[i].max_diskon);
		}

		teks = 'Potongan Member <b>'+potongan_member[i].nama+'</b>';

		keterangan = '* Diskon ' + potongan_member[i].value + ' % | max ' + moneyFormat.to(parseInt(potongan_member[i].max_diskon));


		el = 	'<div  class="card border-main mb-3">'+
			'<div class="card-body" style="padding-left: 5px; padding-right: 5px;">'+
          		'<div>'+teks+' <b class="pull-right">'+
          		'- Rp. <span id="nominal_potongan">'+moneyFormat.to(parseInt(nominal_potongan))+'</span></b><br>'+
        	'<small>'+ keterangan +'</small></div></div></div>';

	}

	$('#potongan_member').html(el);

	return nominal_potongan;
}

