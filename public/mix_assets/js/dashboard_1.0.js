var tahun;
var bulan;
$('#tanggal').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'MM - yy',

	onClose: function() {
		var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
		var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
		bulan = iMonth;
		tahun = iYear;
		$(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	},
	beforeShow: function() {
		if ((selDate = $(this).val()).length > 0)
		{
			iYear = selDate.substring(selDate.length - 4, selDate.length);
			iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5),
				$(this).datepicker('option', 'monthNames'));
			$(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
			$(this).datepicker('setDate', new Date(iYear, iMonth, 1));
		}
	}
});

function reload_table(){
	tabel_penanganan_treatment.ajax.url("dashboard/list_penanganan_treatment").load();
}

function show_data(){
	myChart.destroy();
	bulan = bulan;
	$.ajax({
		url : "dashboard/chart_data/"+bulan+"/"+tahun,
		type: "GET",
		dataType: "JSON",
		success: function(data)
		{
			$('#text_tanggal').text(data.text);
			var label = [];
			var warna = [];
			var data_cart = [];
			$.each(data.data, function(i, item) {
				label.push(data.data[i].tanggal);
				warna.push('rgb(112,47,138)');
				data_cart.push(data.data[i].jumlah);
			});
			myChart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: label,
					datasets: [{
						label: 'Jumlah Order',
						data: data_cart,
						backgroundColor: warna,
						borderWidth: 1
					}]
				},
				options: {
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			});
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			alert('Error get data from ajax');
		}
	});
}

var ctx = document.getElementById('myChart').getContext('2d');
var myChart;

$.ajax({
	url : "dashboard/chart_data/",
	type: "GET",
	dataType: "JSON",
	success: function(data)
	{
		$('#text_tanggal').text(data.text);
		var label = [];
		var warna = [];
		var data_cart = [];
		$.each(data.data, function(i, item) {
			label.push(data.data[i].tanggal);
			warna.push('rgb(112,47,138)');
			data_cart.push(data.data[i].jumlah);
		});
		myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: label,
				datasets: [{
					label: 'Jumlah Order',
					data: data_cart,
					backgroundColor: warna,
					borderWidth: 1
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	},
	error: function (jqXHR, textStatus, errorThrown)
	{
		alert('Error get data from ajax');
	}
});
