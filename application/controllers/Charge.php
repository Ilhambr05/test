<?php
class Charge {
	public function index()
	{
		$server_key = getenv('SERVER_KEY');
    	$api_url = 'https://app.sandbox.midtrans.com/snap/v1/transactions';

		if(getenv("MIDTRANS_IS_PRODUCTION") == "true"){
            $api_url = 'https://app.midtrans.com/snap/v1/transactions';
        }

		if( strpos($_SERVER['REQUEST_URI'], '/charge') != false ) {
			http_response_code(404);
			echo "wrong path, make sure it's `/charge`"; exit();
		}

		if( $_SERVER['REQUEST_METHOD'] !== 'POST'){
			http_response_code(404);
			echo "Page not found or wrong HTTP request method is used"; exit();
		}

		$request_body = file_get_contents('php://input');
		header('Content-Type: application/json');

		$charge_result = $this->chargeAPI($api_url, $server_key, $request_body);

		http_response_code($charge_result['http_code']);

		echo $charge_result['body'];

	}

	public function chargeAPI($api_url, $server_key, $request_body){
		$ch = curl_init();
		$curl_options = array(
			CURLOPT_URL => $api_url,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_POST => 1,
			CURLOPT_HEADER => 0,


			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/json',
				'Accept: application/json',
				'Authorization: Basic ' . base64_encode($server_key . ':')
			),
			CURLOPT_POSTFIELDS => $request_body
		);
		curl_setopt_array($ch, $curl_options);
		$result = array(
			'body' => curl_exec($ch),
			'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
		);
		return $result;
	}
}
?>
