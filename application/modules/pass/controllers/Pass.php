<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 18/09/2018
 * Time: 12.21
 */
class Pass extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Model_pass');
	}

	function change_pass()
	{
		$pass_lama = $this->input->post('pass_lama');

		$lanjut = $this->Model_pass->cek_pass(md5($pass_lama));
		$data_session = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));

		if ($lanjut) {
			$where = array(
				'id_admin' => $data_session['id_unix'],
			);

			$data = array(
				'password' => md5($this->input->post('pass_baru')),
			);
			$res = modules::run('crud/crud/update', 'admin', $where, $data);
			$this->session->unset_userdata(getenv("NAME_SESSION_DASHBOARD"));
			session_destroy();
//			redirect('login');
//			$this->output->set_content_type('application/json')->set_output('Password Diganti');
			echo 'Perubahan Password Berhasil!';
		} else {
//			$this->output->set_content_type('application/json')->set_output('Password Salah');
			echo 'Password yang anda masukan salah!';
		}
	}
}

?>
