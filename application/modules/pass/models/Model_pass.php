<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pass extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function cek_pass($password)
	{
		$data_session = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));

		$this->db->from('admin');
		$this->db->where('id_admin', $data_session['id_unix']);
		$this->db->where('password', $password);
		$this->db->limit(1);

		return $this->db->get()->num_rows();
	}
}
