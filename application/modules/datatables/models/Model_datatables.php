<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 25/09/2018
 * Time: 11.07
 */
class Model_datatables extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    private function _doquery($param)
    {
        //select
        if (isset($param->select)) {
            if (is_object($param->select)) {
                if (isset($param->select->no_quotes)) {
                    $this->db->select($param->select->string, false);
                }
            } else {
                $this->db->select($param->select);
            }
        }

        //tableName
        $this->db->from($param->table);

        // join
        if (isset($param->join)) {
            foreach ($param->join as $j) {
                if (isset($j->align)) {
                    $this->db->join($j->join_table, $j->on, $j->align);
                } else {
                    $this->db->join($j->join_table, $j->on);
                }
                if (isset($j->deleted_hide)) {
                    $this->db->where($j->join_table . ".deleted_at", null);
                }
            }
        }

        //  where
        if (isset($param->where)) {
            foreach ($param->where as $w) {
                if (isset($w->one_string)) {
                    $this->db->where($w->string, null, false);
                } else {
                    $this->db->where($w->param_name, $w->param_value);
                }
            }
        }

        //group by
        if (isset($param->group_by)) {
            $this->db->group_by($param->group_by);
        }

        //search

        $i = 0;
        if (isset($_POST['search'])) {
            foreach ($param->columnSearch as $item) // looping awal
            {
                if ($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
                {
                    if ($i === 0) // looping awal
                    {
                        $this->db->group_start();
                        $this->db->like($item, $_POST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }

                    if (count($param->columnSearch) - 1 == $i)
                        $this->db->group_end();
                }
                $i++;
            }
        }

        // order
        if (isset($_POST['order'])) {
            $this->db->order_by($param->columnOrder[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($param->order_by)) {
            $this->db->order_by($param->order_by);
        }
    }

    function getData($param)
    {
        $this->_doquery($param);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function countFiltered($param)
    {
        //tableName
        $this->db->from($param->table);
        // join
        if (isset($param->join)) {
            foreach ($param->join as $j) {
                if (isset($j->align)) {
                    $this->db->join($j->join_table, $j->on, $j->align);
                } else {
                    $this->db->join($j->join_table, $j->on);
                }
                if (isset($j->deleted_hide)) {
                    $this->db->where($j->join_table . ".deleted_at", null);
                }
            }
        }

        //  where
        if (isset($param->where)) {
            foreach ($param->where as $w) {
                if (isset($w->one_string)) {
                    $this->db->where($w->string, null, false);
                } else {
                    $this->db->where($w->param_name, $w->param_value);
                }
            }
        }

        $i=0;
        if (isset($_POST['search'])) {
            foreach ($param->columnSearch as $item) // looping awal
            {
                if ($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
                {
                    if ($i === 0) // looping awal
                    {
                        $this->db->group_start();
                        $this->db->like($item, $_POST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }

                    if (count($param->columnSearch) - 1 == $i)
                        $this->db->group_end();
                }
                $i++;
            }
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function countAll($param)
    {
        //tableName
//      $this->db->distinct();
        $this->db->from($param->table);

        // join
        if (isset($param->join)) {
            foreach ($param->join as $j) {
                if (isset($j->align)) {
                    $this->db->join($j->join_table, $j->on, $j->align);
                } else {
                    $this->db->join($j->join_table, $j->on);
                }
                if (isset($j->deleted_hide)) {
                    $this->db->where($j->join_table . ".deleted_at", null);
                }
            }
        }
        //  where
        if (isset($param->where)) {
            foreach ($param->where as $w) {
                if (isset($w->one_string)) {
                    $this->db->where($w->string, null, false);
                } else {
                    $this->db->where($w->param_name, $w->param_value);
                }
            }
        }
        return $this->db->count_all_results();
    }
}

?>
