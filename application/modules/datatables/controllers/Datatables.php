<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 25/09/2018
 * Time: 10.53
 */
class Datatables extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_datatables', 'datatables');
    }

    function datatablesData($data)
    {
        $data = $this->datatables->getData($data);
        return $data;
    }

    function countFiltered($data)
    {
        $data = $this->datatables->countFiltered($data);
        return $data;
    }

    function countAll($data)
    {
        $data = $this->datatables->countAll($data);
        return $data;
    }
}

?>