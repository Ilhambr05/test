<?php $this->load->view('admin_template/head.php')?>
<?php $level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD")); ?>

  <!-- <link href="<?php echo base_url('asset/admin_material/select2/select2.min.css'); ?>" rel="stylesheet" /> -->

  <link href="<?php echo base_url('asset/file-upload/css/fileinput.css') ?>" media="all" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
  <link href="<?php echo base_url('asset/file-upload/themes/explorer-fas/theme.css') ?>" media="all" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <body >
    <div class="wrapper ">
      <?php $this->load->view('admin_template/sidebar-material.php')?>
      <div class="main-panel">
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
          <div class="container-fluid">
            <div class="navbar-wrapper">
              <a class="navbar-brand" href="#pablo">Profile Anda</a>
            </div>
          </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
          <section class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-4">
                  <div class="card">
                    <div class="card-header card-header-primary">
                      <h4 class="card-title ">Ubah Password</h4>
                    </div>
                    <div class="card-body">
                      <form onsubmit="return ganti_pass()" id="form_password">
                        <p class="description text-center">Isikan Data</p>
                        <div class="card-body">
                          <div class="form-group mb-3">
                            <label>Password Lama</label>
                            <input name="pass_lama" class="form-control mb-3" type="password" required>
                          </div>
                          <div class="form-group mb-3">
                            <label>Password Baru</label>
                            <input id="password" name="password" class="form-control mb-3" type="password" required>
                          </div>
                          <div class="form-group mb-3">
                            <label>Ketik Ulang Password</label>
                            <input id="retypePassword" name="retypePassword" class="form-control mb-3" type="password" required>
                          </div>
                        </div>
                        <button id="btn_pass" class="btn btn-custom w-100" type="submit" style="border-radius: 0px;">Ubah Password</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
    </div>

    <!-- Modal -->
    <div class="modal fade p-0" id="modal_region" tabindex="-1" role="dialog" aria-labelledby="modal_produk_label">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Form </h4>
          </div>
          <div class="modal-body">
            <form action="#" id="form">
              <input id="id_slider" name="id_slider" type="text" value="" style="display: none;">
              <div class="form-group">
                <label for="nama" class="label-control label-custom">Title :</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Masukan Title Slider" value="" >
              </div>
              <div class="form-group">
                <label for="recipient-name" class="label-control label-custom">Pilih Foto :</label>
                <label class="btn btn-rose btn-round btn-file" for="foto">
                  <input id="foto" accept="image/*" type="file" onchange="$('#upload-file-info').html(this.files[0].name)" hidden>
                  Pilih Foto
                </label>
                <span id="upload-file-info" name="nama_foto"></span>
                <br>
                <img name="foto_tampil" class="img-thumbnail" src="" alt="" >
              </div>
            </form>
          </div>
          <div id="footer"class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Submit</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </body>
<!--   Core JS Files   -->
<?php $this->load->view('admin_template/js.php')?>
<script type="text/javascript" src="<?php echo site_url('mix_assets/js/profile_'.getenv("PROFILE").'.js'); ?>"></script>
<?php
$this->load->view('admin_template/foot');
?>
