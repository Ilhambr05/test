<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper("URL", "DATE", "URI", "FORM");
		$this->load->helper('security');
		$this->load->library('session');
        $this->load->library('Date');
	}

	public function index()
	{
		$this->load->view('index');
	}
	
	function change_pass()
    {
    	$level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));

        $pass_lama = $this->input->post('pass_lama');

        //cocokkan dengan password lama
        $value = (object) array(
            'table' => 'admin', //wajib
            'select' => (object) array( //wajib
                'string' => "password",
                'no_quotes' => true,
            ),
            'where' => array( //opsional
                (object) array(
                    'param_name' => 'password',
                    'param_value' => md5($pass_lama),
                ),
                (object) array(
                    'param_name' => 'id_admin',
                    'param_value' => $level['id_unix'],
                ),
            ),
            'count' => true,
        );

        $lanjut = modules::run('crud/crud/get', $value);

        if ($lanjut) {
            $where = array(
                'id_admin' => $level['id_unix'],
            );

            $data = array(
                'password' => md5($this->input->post('password')),
            );
            $res['response'] = modules::run('crud/crud/update', 'admin', $where, $data);

            $res['message'] = 'Password diubah, silahkan login kembali.';

            $this->session->unset_userdata(getenv("NAME_SESSION_DASHBOARD"));
            // session_destroy();
			$this->output->set_content_type('application/json')->set_output(json_encode($res));
        } else {
        	$res['response'] = false;

            $res['message'] = 'Password yang anda masukan salah.';

            $this->output->set_content_type('application/json')->set_output(json_encode($res));
        }
    }
}