<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Landing extends MY_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
        $data = $this->get_data();

		$this->load->view('index', $data);
	}

    public function get_data()
    {
        $path_img_produk = site_url('asset/img/produk/');

        $level = $this->session->userdata(getenv("NAME_SESSION_MEMBER"));

        // ambil data semua produk jenis satuan
        $value = (object) array(
            'table' => 'produk', //wajib
            'select' => (object) array( //wajib
                'string' => "id, nama, harga, deskripsi, foto",
                'no_quotes' => true,
            ),
            'where' => array( //opsional
                (object) array(
                    'param_name' => 'status_delete',
                    'param_value' => 0,
                ),
            ),
        );

        $data['produk'] = modules::run('crud/crud/get', $value);

		foreach ($data['produk'] as $key => $value) {
			$value->foto = $path_img_produk.$value->foto;
		}

        // echo $this->db->last_query();
        // echo '<pre>';
        // print_r($data['imgs'][7]);
        // echo '</pre>';

        return $data;
    }

	public function proses_pemesanan()
	{
		// echo '<pre>';
		// var_dump(json_decode($this->input->post('cart')));
		// echo '</pre>';
		
		// insert data pemesan
		$cart = json_decode($this->input->post('cart'));
		$grand_total = 0;
		foreach ($cart as $key => $value) {
			$grand_total += $value->harga * $value->jml;
		}
		$kode_transaksi = str_replace(' ', '_', $this->input->post('atas_nama'))."_".date('dmy');

		$input = array(
			"kode_pesan"=> $kode_transaksi,
			"atas_nama" => $this->input->post('atas_nama'),
			"email" 	=> $this->input->post('email'),
			"no_hp" 	=> $this->input->post('no_hp'),
			"grand_total"	=> $grand_total,
			"create_by"	=> 0,
		);
		$res['insert_pemesanan'] = modules::run('crud/crud/insert', 'pemesanan', $input);

		$id_pemesanan = $this->db->insert_id();

		if($res['insert_pemesanan']){
			foreach ($cart as $key => $value) {
				$input = array(
					"id_pemesanan"=> $id_pemesanan,
					"nama_item" => $value->nama,
					"jml" 	=> $value->jml,
					"harga" 	=> $value->harga,
					"create_by"	=> 0,
				);
	
				$res['insert_detail_pemesanan'] = modules::run('crud/crud/insert', 'detail_pemesanan', $input);
			}
		}

		if($res['insert_pemesanan'] && $res['insert_detail_pemesanan']){
			$res['insert'] = true;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

    public function logout()
	{
		$this->session->unset_userdata(getenv("NAME_SESSION_MEMBER"));
		// session_destroy();
		redirect('landing');
	}

	public function hapus_cookie()
	{
		setcookie('FmediaKaraoke-cart', '', time() - 3600, '/');
		setcookie('FmediaKaraoke-cart-treatment', '', time() - 3600, '/');
		setcookie('FmediaKaraoke-cart-lokasi', '', time() - 3600, '/');
	}
}
?>
