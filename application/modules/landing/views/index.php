<?php $this->load->view('template/top-html'); ?>

<style type="text/css">
	.full-screen {
		background-size: cover;
		background-position: center center;
		background-repeat: no-repeat;
	}

	.carousel-caption {
		top: 35%;
	}

	#modal-lokasi {
		scroll-behavior: smooth;
	}

	.deskripsi-singkat {
		overflow: hidden;
		display: -webkit-box;
		-webkit-line-clamp: 4;
		-webkit-box-orient: vertical;
	}
</style>

</head>

<body style="padding-right: 0px !important;">
	<div class="loader">
		<div class="preview" style="background: rgba(255, 255,255,0.7) url('impressions_asset/fonts/oval.svg') center center no-repeat; background-size:125px;"></div>
	</div>

	<?php $this->load->view('template/head-navbar'); ?>

	<div class="content">
		<main class="main">
			<div class="section">
				<div class="container">
					<div class="tab-content mt-2">
						<section id="list_produk">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="top-text">
										<h2>List Produk</h2>
									</div>
								</div>
							</div>

							<div class="row">
								<?php foreach ($produk as $value) { ?>
									<div class="col-xs-12 col-sm-6 col-md-3 list_item" data-id="<?php echo $value->id ?>" tipe-item='produk'>
										<div class="card no-border p-0 mb-3">
											<div class="detail-btn h-100" onclick="detail(<?php echo $value->id ?>, 'produk');"></div>
											<div class="ket_jml text-center text-white row align-items-center m-0" style="display: none;">
												<h1 class="col w-100">
													+<span class="val_jml"></span>
												</h1>
											</div>
											<div class="container-img">

												<div class="img_item" style="background-image: url('<?php echo $value->foto ?>');"></div>
												<foto_item style="display:none"><?php echo $value->foto ?></foto_item>
											</div>
											<div class="card-body pt-2 pr-3 pb-2 pl-3">
												<p class="text-center"><b><nama><?php echo $value->nama ?><nama></b></p>
												<p class="text-center">
													<b>Rp. <harga><?php echo number_format($value->harga, 0, ",", "."); ?></harga></b>
												</p>
												<br>
												<div class="card-text text-muted"><?php echo substr($value->deskripsi, 0, 75) ?> ... </div>
												<!-- <p class="card-text text-success text-ellipsis text-promo"><promo>beli 1 gratis semua atau sebagainya lah atau gimana terserah</promo></p> -->
												<deskripsi style="display: none;"><?php echo $value->deskripsi ?></deskripsi>
											</div>
											<div class="footer-beli" style="max-height: 50px;">
												<div class="btn_beli">
													<button class="btn btn-custom btn-lg w-100 m-0" onclick="beli(<?php echo $value->id ?>,this, 'produk')">
														<i class="material-icons" style=" top: -2px;">shopping_cart</i>
														Beli
													</button>
												</div>
												<div class="row m-1 jml_beli" style="display: none;">
													<div class="w-50 m-0 p-1"><button class="btn btn-success w-100 m-0" onclick="add_cart(<?php echo $value->id ?>, 'produk')"><i class="material-icons">exposure_plus_1</i> </button></div>
													<div class="w-50 m-0 p-1"><button class="btn btn-danger w-100 m-0" onclick="rm_cart(<?php echo $value->id ?>,this,0, 'produk')"><i class="material-icons icon_del">highlight_off</i> </button></div>
												</div>
											</div>
										</div>
									</div>
									<!-- end template item -->
								<?php } ?>
							</div>
						</section>
					</div>
				</div>
			</div>


		</main>
	</div>

	<?php $this->load->view('template/footer'); ?>

	<!-- Modal untuk cart -->
	<div class="modal fade" id="modal-cart" tabindex="-1" role="dialog">
		<div class="modal-dialog --modal-cart" role="document">
			<div class="modal-content">
				<div class="modal-header pt-3 no-border">
					<h4 class="modal-title text-black">List Belanja <strong>( Total Rp. <span class="total_cart">0</span> )</strong></h4>
					<button type="button" class="btn btn-sm btn-close btn-danger" data-dismiss="modal" aria-label="Close">
						Tutup
					</button>
				</div>
				<div class="modal-body pt-2">
					<div id="list_cart">
						<!-- auto gen -->
					</div>
					<hr>
					<h4 class="d-flex justify-content-end align-items-center p-1">
						<small>Grand Total</small>&nbsp Rp. &nbsp<strong class="total_cart">0</strong>
					</h4>
					<hr class="mb-0">
				</div>
				<form onsubmit="proses_transaksi()" id="form">
					<div class="modal-body pt-0">
						<h4 class="modal-title text-black">Data Pemesan</h4>
						<br>
						<div class="form-group">
							<label for="nama_member">Atas Nama*</label>
							<input type="text" class="form-control" name="atas_nama" placeholder="Nama Pemesan" value="" required>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6">
								<div class="form-group">
									<label for="email">Email*</label>
									<input type="email" class="form-control" placeholder="Email Pemesan" name="email" value="" required>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6">
								<div class="form-group">
									<label for="no_hp">No Hp*</label>
									<input type="text" class="form-control" placeholder="Nomor HP Pemesan" name="no_hp" value="" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer p-3 no-border">
						<!-- <p class="text-warning">Sistem checkout masih dalam perbaikan. mohon coba lagi nanti.</p> -->
						<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Tutup</button>
						<button type="submit" id="btn-checkout" class="btn btn-success"><i class="material-icons" style="top: -2px !important;">done</i> &nbsp Proses Pemesanan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--  End Modal -->

	<!-- template untuk list cart -->
	<template id="template_list_cart">
		<div class="card card-cart mt-2 mb-2 img-raised" style="overflow: hidden;" data-id="{{id_item}}" tipe-item='{{tipe-item}}'>
			<stok style="display: none;">{{stok}}</stok>
			<div class="card-body p-0 row">
				<img class="img-raised hidden-xs hidden-sm col-md-3 pr-0" alt="Img item" src="{{image}}">
				<!-- <div class="img-raised col-md-3 pr-0" style="background-image: url('{{image}}');"></div> -->
				<div class="col-xs-12 col-sm-12 col-md-9">
					<button class="btn btn-danger btn-sm btn-mini p-1 del-item-cart" onclick="rm_cart('{{id_item}}',this,1,'{{tipe-item}}')">
						<i class="material-icons">delete_outline</i>
					</button>
					<div class="p-2 d-flex flex-column h-100">
						<p><b>{{nama_item}}</b></p>
						<p class="card-text">
							@ Rp. {{harga}}
							<?php
							//if($rev['nama']){ // jika ada reveral aktif
							?>
							<!-- <span class="text-danger text-center">{{potongan_reveral}}</span> -->
							<?php //} 
							?>
						</p>
						<!-- <p class="card-text text-success">{{promo}}</p> -->
						<div class="mt-auto handler_jumlah_item_cart">
							<span id="total_item_{{id_item}}" class="p-2"><strong>X {{jml}}</strong></span>
							<div class="pull-right"><b>Rp. <span id="total_harga_{{id_item}}">{{total}}</span></b></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</template>

	<!-- Modal untuk detail item -->
	<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header pt-3 no-border">
					<h5 class="modal-title text-black">Detail Item</h5>
				</div>
				<div class="modal-body w-100">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4">
							<div class="img_item rounded img-raised"></div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-8">
							<h3 class="m-0 detail_nama">
								<nama></nama>
							</h3>
							<hr>
							<p><b>Rp. <harga></harga></b>
								<satuan class="card-text text-muted"></satuan>
							</p>
							<p class="card-text text-success">
								<promo></promo>
							</p>
							<p class="card-text">
								<deskripsi></deskripsi>
							</p>
						</div>
					</div>
				</div>
				<div class="modal-footer no-border">
					<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" aria-label="Close">
						Tutup
					</button>
				</div>
			</div>
		</div>
	</div>
	<!--  End Modal -->
	<button id="checkout" class="btn btn-info" onclick="show_cart_list()">
		<div>
			<i class="material-icons">shopping_cart</i>
			<label class="badge badge-pill badge-success" style="margin: 0px;"><span id="jml_cart"></span></label>
			&nbsp | &nbsp Rp.
			<span class="total_cart">0</span>
		</div>
	</button>

	<script src="<?php echo site_url('impressions_asset/js/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo site_url('impressions_asset/js/imagesloaded.pkgd.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo site_url('impressions_asset/js/formValidation.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo site_url('impressions_asset/js/wow.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo site_url('impressions_asset/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo site_url('impressions_asset/js/jquery.sticky.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo site_url('impressions_asset/js/bootstrap-datepicker.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo site_url('impressions_asset/js/premedi_custom.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo site_url('impressions_asset/js/owl.carousel.min.js'); ?>"></script>

	<script type="text/javascript" src="<?php echo site_url('impressions_asset/js/jquery.cookie.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo site_url('asset/wNumb.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo site_url('asset/select2/select2.full.min.js'); ?>"></script>

	<script>
		// beberapa value untuk kegunaan script ini harus di set sebelumnya di file berbeda.

		function set_rasio_img() {
			// var w = $(".img_item").width();
			// $(".img_item, .detail-btn, .ket_jml").css({ 'height': w + "px" });

			// var w = $("#list_cart img").width();
			// $("#list_cart img").css({ 'height': w + "px" });
		}

		var waitForFinalEvent = (function() {
			//delay sampai event selesai execute
			var timers = {};
			return function(callback, ms, uniqueId) {
				if (!uniqueId) {
					uniqueId = "Don't call this twice without a uniqueId";
				}
				if (timers[uniqueId]) {
					clearTimeout(timers[uniqueId]);
				}
				timers[uniqueId] = setTimeout(callback, ms);
			};
		})();

		$(window).resize(function() {
			// panggil ketika screen size berubah
			waitForFinalEvent(function() {
				set_rasio_img();
			}, 500, "win resize selesai");
		});

		var moneyFormat = wNumb({
			mark: ',',
			decimals: 0,
			thousand: '.',
			prefix: '',
			suffix: ''
		});

		$('a[data-toggle="pill"]').on('hide.bs.tab', function(e) {
			//scroll ke atas jika pill di tekan.
			$('html, body').animate({
				scrollTop: $('.section').offset().top - $('#sticky_navigation').outerHeight()
			}, 0);
			// console.log(e.target); // newly activated tab
			// console.log(e.relatedTarget); // previous active tab
		})

		var cart_produk = get_data_cart('produk');

		$(document).ready(function() {
			//$.removeCookie('Impressions-cart');

			// panggil di page jika document sudah siap
			// *tak masalah document.ready dipanggil lebih dari sekali di satu page
			init_item_selected();
		});

		function get_data_cart(tipe) {
			if ($.cookie('Impressions-cart')) {
				return JSON.parse($.cookie('Impressions-cart'));
			} else {
				return [];
			}
		}

		function init_item_selected() {
			//membuat tampilan item yang sudah masuk di cart

			var nominal = 0;

			for (var i in cart_produk) {
				var el = $('[data-id="' + cart_produk[i].id + '"][tipe-item="produk"]');
				el.find('.ket_jml').show();
				if (cart_produk[i].jml > 1) {
					el.find('.icon_del').text('exposure_minus_1');
				}
				el.find('.val_jml').text(cart_produk[i].jml);
				$('#modal-cart').find('#total_item_' + cart_produk[i].id).text(cart_produk[i].jml);

				el.find('.btn_beli').hide();
				el.find('.jml_beli').show();

				var harga = el.find('harga').text();
				harga = parseInt(harga.replace(/\./g, ''));

				if (cart_produk[i].jml >= cart_produk[i].stok) {
					el.find('.jml_beli .btn-success').prop('disabled', true);
				} else {
					el.find('.jml_beli .btn-success').prop('disabled', false);
				}

				nominal += cart_produk[i].jml * harga;
			}

			$('#jml_cart').text(cart_produk.length);

			hit_total();
		}

		function detail(id, tipe) {
			var selector = $('[data-id="' + id + '"][tipe-item="' + tipe + '"]');

			$('#modal-detail nama').text(selector.find('nama').text());
			$('#modal-detail harga').text(selector.find('harga').text());
			$('#modal-detail satuan').text(selector.find('satuan').text());
			$('#modal-detail promo').text(selector.find('promo').text());
			$('#modal-detail deskripsi').html(selector.find('deskripsi').html());
			$('#modal-detail .img_item').css('background-image', "url('" + selector.find('foto_item').text() + "')");

			// var tipe = selector.attr('tipe-item');

			$('#modal-detail').modal('show');
		}

		function beli(id_item, btn, tipe) {
			$(btn).parent().hide();
			$(btn).parent().siblings('.jml_beli').show();

			add_cart(id_item, tipe);
		}

		function add_cart(id_item, tipe) {
			atur_view_add(id_item, tipe);
		}

		function atur_view_add(id_item, tipe) {
			var exist = 0;
			var jml_arr = 1;

			if (tipe == 'produk') {
				var el = $('[data-id="' + id_item + '"][tipe-item="produk"]');

				console.log('selector utama');
				console.log(el);

				el.find('.ket_jml').show();

				for (var i in cart_produk) {
					if (cart_produk[i].id == id_item) {

						jml_arr = parseInt(cart_produk[i].jml) + 1;
						cart_produk[i].jml = jml_arr;

						exist = 1;
						el.find('.icon_del').text('exposure_minus_1');

						var nominal = 0;
						nominal += cart_produk[i].jml * cart_produk[i].harga;

						el.find('#total_harga_' + id_item).text(moneyFormat.to(nominal));

						if (jml_arr >= cart_produk[i].stok) {
							// disable button untuk nambah di cart produk
							el.find('.jml_beli .btn-success').prop('disabled', true);

							// disable button untuk nambah di cart produk
							el.find('.handler_jumlah_item_cart .btn-success').prop('disabled', true);
						}

						break;
						//Stop loop
					}
				}

				el.find('.icon_del').text('exposure_minus_1');

				el.find('.val_jml').text(jml_arr);
				el.find('#total_item_' + id_item).text(jml_arr);

				if (!exist) {
					var nama = el.find('nama').text();
					// var image = el.find('img').prop('src');
					var image = el.find('img_item').text();
					var harga = el.find('harga').text();
					var stok = el.find('stok').text();
					var tipe_produk = el.attr('tipe-produk');
					var id_mp = el.attr('id-master-produk');
					harga = parseInt(harga.replace(/\./g, ''));

					var obj = {
						id: id_item,
						jml: 1,
						nama: nama,
						harga: harga
					};

					cart_produk.push(obj);
					console.log('tambah item cart lokal - cookie');
					//tambah obj
				}

				console.log('jml_arr ' + jml_arr);

			}

			// console.log(jml_arr);

			proses_lanjutan();
		}

		function rm_cart(id_item, btn, remove = 0, tipe) {
			atur_view_rm(id_item, btn, remove, tipe);
		}

		function atur_view_rm(id_item, btn, remove, tipe) {
			var hide = 0;
			var jml_arr = 1;

			// console.log('remove '+remove);
			if (tipe == 'produk') {

				var el = $('[data-id="' + id_item + '"][tipe-item="produk"]');
				for (var i in cart_produk) {
					if (cart_produk[i].id == id_item) {

						cart_produk[i].jml = jml_arr = parseInt(cart_produk[i].jml) - 1;
						// jml_arr = cart_produk[i].jml;

						var nominal = 0;

						nominal += parseInt(cart_produk[i].jml) * parseInt(cart_produk[i].harga);

						if (remove) {
							cart_produk[i].jml = 0;
							hide = 1;
						}
						if (jml_arr <= 1) {
							el.find('.icon_del').text('highlight_off');
						}
						if (jml_arr <= 0) {
							//remove arr index
							cart_produk.splice(i, 1);
							hide = 1;
							jml_arr = 0;
							$(btn).closest('.card-cart').remove();
						}

						el.find('#total_harga_' + id_item).text(moneyFormat.to(nominal));

						break;
						//Stop loop
					}
				}

				el.find('.val_jml').text(jml_arr);
				el.find('#total_item_' + id_item).text(jml_arr);

				if (hide) {
					el.find('.ket_jml, .jml_beli').hide();
					$(btn).closest('.card-cart').remove();
					el.find('.btn_beli').show();
				}

				if (jml_arr != el.find('stok').text()) {
					// un-disable button untuk nambah di list produk
					el.find('.jml_beli .btn-success').prop('disabled', false);

					// un-disable button untuk nambah di cart produk
					el.find('.handler_jumlah_item_cart .btn-success').prop('disabled', false);
				}

			}

			proses_lanjutan();
		}

		function proses_lanjutan() {
			//masukkan ke cookie
			$.cookie('Impressions-cart', JSON.stringify(cart_produk), {
				path: '/'
			});

			$('#jml_cart').text(cart_produk.length);

			hit_total();

			//jika isi cart kosong
			if (!cart_produk.length) {
				$('#modal-cart').modal('hide');
			}
		}

		function hit_total() {
			var nominal = 0;
			for (var i in cart_produk) {
				nominal += cart_produk[i].jml * cart_produk[i].harga;
			}

			$('.total_cart').text(moneyFormat.to(nominal));
		}

		function gen_view(id) {
			//masukkan ke cookie
			// $.cookie('Impressions-cart', JSON.stringify(cart_produk), { path: '/' });
		}

		function show_cart_list() {
			$('#modal-cart').modal('show');
		}

		function init_list_cart() {
			$('#list_cart').empty();

			$('#btn-checkout').hide();

			// proses list produk / paket produk
			if (cart_produk.length) {
				$('#list_cart').append('<h4>Produk</h4>');
			}

			for (var i in cart_produk) {
				console.log(i);
				var list = $('#template_list_cart').clone();

				var new_html = list[0].innerHTML;
				//id_item dan tipe-item secara global (regex?) akan di replace, lainnya single replace.
				new_html = new_html.replace(/{{id_item}}/g, cart_produk[i].id);
				new_html = new_html.replace(/{{tipe-item}}/g, 'produk');
				new_html = new_html.replace('{{nama_item}}', cart_produk[i].nama);
				new_html = new_html.replace('{{harga}}', moneyFormat.to(parseInt(cart_produk[i].harga)));

				// set image pada list
				var img_template = $('[data-id="' + cart_produk[i].id + '"]').find('foto_item').text();
				new_html = new_html.replace('{{image}}', img_template);

				new_html = new_html.replace('{{jml}}', cart_produk[i].jml);

				var nominal = 0;
				nominal += cart_produk[i].jml * cart_produk[i].harga;

				new_html = new_html.replace('{{total}}', moneyFormat.to(nominal));

				//jika qty = stok -> disable btn tambah
				if (cart_produk[i].jml >= cart_produk[i].stok) {
					new_html = new_html.replace('{{disabled}}', 'disabled');
				}

				// console.log(cart_produk[i].nama);
				// list.replace('{{nama_item}}', cart_produk[i].nama);
				// console.log(list);
				$('#list_cart').append(new_html);

				$('#btn-checkout').show();
			}
		}

		$("#modal-cart").on('show.bs.modal', function() {
			init_list_cart();
			// var w = $("#list_cart img").width();
			// console.log(w);
			// $("#list_cart img").css({ 'height': w + "px" });
		});
		$("#modal-cart").on('shown.bs.modal', function() {
			// var w = $("#list_cart img").width();
			// $("#list_cart img").css({ 'height': w + "px" });
		});

		function proses_transaksi() {
			event.preventDefault();

			var site_url = "<?php echo site_url('landing/proses_pemesanan') ?>";
			$('#btnSave').attr('disabled', true); //set button disable

			var form_data = new FormData(document.getElementById("form"));
			form_data.set('cart', JSON.stringify(cart_produk));

			$.ajax({
				url: site_url,
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					data = JSON.parse(data);
					if (data.insert) //if success close modal and reload ajax table
					{
						alert('Pemesanan sedang di proses. kami akan mengkonfirmasi pemesanan pada nomor hp dan email yang anda inputkan.');
						$('#modal-cart').modal('hide');
					} else {
						alert("Terjadi Kesalahan, Silahkan Coba lagi!")
					}
					$('#btnSave').attr('disabled', false); //set button enable
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert('Error adding / update data');
					$('#btnSave').attr('disabled', false); //set button enable
				}
			});
		}
	</script>
</body>

</html>
