<?php $this->load->view('admin_template/head.php')?>
<?php $level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD")); ?>

<!-- <link href="<?php echo base_url('asset/admin_material/select2/select2.min.css'); ?>" rel="stylesheet" /> -->

<link href="<?php echo base_url('asset/file-upload/css/fileinput.css') ?>" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
<link href="<?php echo base_url('asset/file-upload/themes/explorer-fas/theme.css') ?>" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
	.label-custom{
		position: unset !important;
	}
	.file-input .clearfix::after {
		display: table !important;
	}
	.file-drop-zone{
		padding: 0px;
		margin: 0px;
	}
	.fileinput-remove{
		display: none;
	}
	.file-preview-frame {
		background:white;
	}
	.file-footer-caption{
		margin-bottom: 15px;
	}
	.file-footer-caption div{
		height: 18px !important;
	}
	.btn .fa{
		top: -2px !important;
	}
	.material-icons, :not(.btn-tambah .material-icons){
		top: 2px !important;
	}
	.btn-tambah{
		position: absolute;
		top: 0px;
		right: 5px;
	}
	.w-50{
		width: 50% !important;
		margin: 5px -1px;
	}
	.select2-container--default .select2-selection--single{
		height: calc(2.4375rem + 2px);
		padding: 7px 0px;
		border-radius: 0px;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow {
		height: 35px;
	}
</style>

<body class="">
	<div class="wrapper ">
		<?php $this->load->view('admin_template/sidebar-material.php')?>
		<div class="main-panel">
			<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
				<div class="container-fluid">
					<div class="navbar-wrapper">
						<a class="navbar-brand" href="#pablo">Manage Produk</a>
					</div>
				</div>
			</nav>
			<!-- End Navbar -->
			<div class="content">
				<section class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-header card-header-primary">
										<h4 class="card-title ">Data Produk &nbsp

										<?php if($level['level'] == 'super admin'){ ?> 
											<button class="btn btn-success" onclick="tambah()"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
										<?php } ?>

										</h4>
									</div>
									<div class="card-body">
										<div class="toolbar"></div>
										<div class="material-datatables">
											<table class="table" id="example1">
												<thead>
													<tr>
														<th>Nama</th>
														<th>Harga Jual</th>
														<th>Deskripsi</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade p-0" id="modal_produk" tabindex="-1" role="dialog" aria-labelledby="modal_produk_label">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Form Stok Produk</h4>
				</div>
				<form onsubmit="save()" id="form">
					<div class="modal-body">
						<input id="id" name="id" type="text" value="" style="display: none;">
						<div class="form-group">
							<label for="nama" class="label-control label-custom">Nama Produk :</label>
							<input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan Nama Produk" value="" required autofocus>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="label-control label-custom">Harga Jual :</label>
							<input type="text" class="form-control" name="harga" id="harga" placeholder="Rp. --" value="" required>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="label-control label-custom">Deskripsi Produk :</label>
							<textarea name="deskripsi" class="form-control ckeditor" rows="10" required></textarea>
						</div>
						<br>
						<div class="form-group">
							<label for="recipient-name" class="label-control label-custom">Pilih Foto :</label>
							<label class="btn btn-rose btn-round btn-file" for="image">
								<input id="image" accept="image/*" type="file" onchange="$('#upload-file-info').html(this.files[0].name)" required>
								Pilih Foto
							</label>
							<span id="upload-file-info" name="nama_foto"></span>
							<br>
							<img name="image_tampil" class="img-thumbnail" src="" alt="" >
						</div>
						<hr>
					</div>
					<div id="footer"class="modal-footer">	  
						<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						<button type="submit" id="btnSave" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<!--   Core JS Files   -->
	<?php $this->load->view('admin_template/js.php')?>

	<!--tambahkan custom js disini-->
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<!-- <script src="<?php echo base_url('asset/admin_material/select2/select2.full.min.js'); ?>"></script> -->


	<script>
	// $('#master_jenis').select2();
	$(document).ready(function(){
		//
	});

	(function ($) {
		$.fn.simpleMoneyFormat = function() {
			this.each(function(index, el) {		
				var elType = null; 
				var value = null;
			// get value
			if($(el).is('input') || $(el).is('textarea')){
				value = $(el).val().replace(/\./g, '');
				elType = 'input';
			} else {
				value = $(el).text().replace(/\./g, '');
				elType = 'other';
			}
			// if value changes
			$(el).on('paste keyup', function(){
				value = $(el).val().replace(/\./g, '');
				formatElement(el, elType, value);
				// format element
			});
			formatElement(el, elType, value); 
			// format element
		});
		};
	}(jQuery));

	function formatElement(el, elType, value){
		var result = '';
		var valueArray = value.split('');
		var resultArray = [];
		var counter = 0;
		var temp = '';
		for (var i = valueArray.length - 1; i >= 0; i--) {
			temp += valueArray[i];
			counter++
			if(counter == 3){
				resultArray.push(temp);
				counter = 0;
				temp = '';
			}
		};
		if(counter > 0){
			resultArray.push(temp);             
		}
		for (var i = resultArray.length - 1; i >= 0; i--) {
			var resTemp = resultArray[i].split('');
			for (var j = resTemp.length - 1; j >= 0; j--) {
				result += resTemp[j];
			};
			if(i > 0){
				result += '.'
			}
		};
		if(elType == 'input'){
			$(el).val(result);
		} else {
			$(el).empty().text(result);
		}
	}

	$('#harga').simpleMoneyFormat();

	$(function () {
		table = $('#example1').DataTable({
			// Load data for the table's content from an Ajax source
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			'responsive': true,
			"ajax": {
				"url": "<?php echo site_url('manage_produk/ajax_list')?>",
				"type": "POST"
			},
			"order": [], //Initial no order.
		});
	});

	function edit(id)
	{
		save_method = 'update';
	    $('#form')[0].reset(); // reset form on modals
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string
	    $('[name="foto_tampil"]').show();
	    //Ajax Load data from ajax
	    $.ajax({
	    	url : "<?php echo site_url('manage_produk/edit')?>/" + id,
	    	type: "GET",
	    	dataType: "JSON",
	    	success: function(data)
	    	{
	    		$('[name="id"]').val(data[0].id);
	    		$('[name="nama"]').val(data[0].nama);
	    		$('[name="harga"]').val(data[0].harga);
	    		$('[name="deskripsi"]').val(data[0].deskripsi);
					
					$('[name="image_tampil"]').attr('src', data[0].foto).show();

	    		$('#harga').simpleMoneyFormat();

	            $('#modal_produk').modal('show'); // show bootstrap modal when complete loaded
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	        	alert('Error get data from ajax');
	        }
	    });
	}

	function reload_table()
	{
	    table.ajax.reload(null,false); //reload datatable ajax 
	}

	function del(id)
	{
		if(confirm('Apakah anda yakin akan menghapus data?'))
		{
	        // ajax delete data to database
	        $.ajax({
	        	url : "<?php echo site_url('manage_produk/delete_produk')?>/"+id,
	        	type: "POST",
	        	dataType: "JSON",
	        	success: function(data)
	        	{
	                //if success reload ajax table
	                //$('#modal_form').modal('hide');
	                reload_table();
	            },
	            error: function (jqXHR, textStatus, errorThrown)
	            {
	            	alert('Error deleting data');
	            }
	        });

	    }
	}

	function tambah()
	{
		save_method = 'add';
	    $('#form')[0].reset(); // reset form on modals
	    $('#modal_produk').modal('show'); // show bootstrap modal
	    $('.modal-title').text('Tambah Produk'); // Set Title to Bootstrap modal title
		$('[name="image_tampil"]').attr('src', '').hide();
	}

	$('#modal_produk').on('shown.bs.modal', function (e) {
		$('input[autofocus]').focus();
		$('#upload-file-info').html('');
	})

	function save() {
		event.preventDefault();

		var site_url = "<?php echo site_url('manage_produk/insert_item') ?>";
	    $('#btnSave').attr('disabled', true); //set button disable

	    $('#btnSave').attr('disabled',true); //set button disable 

			var file_data = $('#image').prop('files')[0];

	    var form_data = new FormData(document.getElementById("form"));

	    form_data.set('harga', parseInt($('#harga').val().replace(/\./g,'')));
			form_data.set('foto',file_data);

	    if (save_method == 'update') {
	    	site_url = "<?php echo site_url('manage_produk/proses_edit_item') ?>";
	        // form_data.append('id', $('[name="id"]').val());
	    }

	    $.ajax({
	    	url : site_url,
	        dataType: 'text',  // what to expect back from the PHP script, if anything
	        cache: false,
	        contentType: false,
	        processData: false,
	        data: form_data,
	        type: 'post',
	        success: function(data)
	        {
	        	data = JSON.parse(data);
	        	if(data.nama_produk_exist) //if success close modal and reload ajax table
	        	{
							alert('Nama Produk Sudah Digunakan');
							$('[name="nama"]').focus();
						}
	        	else if(data.insert || data.update) //if success close modal and reload ajax table
	        	{
							reload_table();
							$('#modal_produk').modal('hide');
				}
				else{
					alert("Terjadi Kesalahan, Silahkan Coba lagi!")
				}
				$('#btnSave').attr('disabled', false); //set button enable
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error adding / update data');
				$('#btnSave').attr('disabled',false); //set button enable
			}
		});
	}

</script>
<?php
$this->load->view('admin_template/foot');
?>
