<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_produk extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper("URL", "DATE", "URI", "FORM");
		$this->load->library('session');
        $this->load->library('Date');
	}

	public function index()
	{
        //cek leveling baru
        $this->load->library('leveling');
        $this->leveling->cek_level('manage produk');

	   	$this->load->view('index');
	}
	
	public function ajax_list()
    {
        //cek leveling baru
        $this->load->library('leveling');
        $this->leveling->cek_level('manage produk');

        $level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));

	 	$value = (object) array(
            'table' => 'produk', //wajib
            'select' => (object) array( //wajib
                'string' => "id, nama, harga, deskripsi",
                'no_quotes' => true,
            ),
            'where' => array( //opsional
                (object) array(
                    'param_name' => 'status_delete',
                    'param_value' => 0,
                ),
            ),
            'columnSearch' => array('nama', 'harga', 'deskripsi'),
            'columnOrder' => array('nama', 'harga', 'deskripsi', null), //wajib
            'order_by' => "id DESC", //wajib
        );
        $data = modules::run('datatables/datatables/datatablesData', $value);

        $list = $data;
        $data = array();
        foreach ($list as $lihat) {
            $row = array();

            $row[] = $lihat->nama;
            $row[] = 'Rp. '.number_format($lihat->harga,0,"",".");
            $row[] = substr($lihat->deskripsi,0,75);
            $row[] = '
            <a class="btn btn-sm btn-info" title="Edit" onclick="edit(' . "'" . $lihat->id . "'" . ')">Edit/detail</a>
            <a class="btn btn-sm btn-danger" title="Delete" onclick="del(' . "'" . $lihat->id . "'" . ')">Delete</a>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => modules::run('datatables/datatables/countAll', $value),
            "recordsFiltered" => modules::run('datatables/datatables/countFiltered', $value),
            "data" => $data,
        );
        //output to json format
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
    }
	
	public function edit($id)
	{
        //cek leveling baru
        $this->load->library('leveling');
        $this->leveling->cek_level('manage produk');

        $level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));

		$cek_param = (object)array(
            'table' => 'produk',
            'select' => (object)array(
                'string' => "id, nama, harga, deskripsi, foto",
                'no_quotes' => true
            ),
            'where' => array(       //opsional
                (object) array(
                    'param_name' => 'status_delete',
                    'param_value' => 0,
                ),
                (object) array(
                    'param_name' => 'id',
                    'param_value' => $id,
                ),
            ),
        );
        $res = modules::run('crud/crud/get', $cek_param);

		foreach ($res as $key => $value) {
			$value->foto = 'asset/img/produk/'.$value->foto;
		}

        $this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

	public function delete_produk($id)
    {
        //cek leveling baru
        $this->load->library('leveling');
        $this->leveling->cek_level('manage produk');

        $where = array(
            'id' => $id,
        );

        $res = modules::run('crud/crud/delete', 'produk', $where);
		
        $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
	
	public function insert_item()
	{
        //cek leveling baru
        $this->load->library('leveling');
        $this->leveling->cek_level('manage produk');

		$level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));

		// init input data
		$input = $this->input->post();
		unset($input['id_produk']);
		unset($input['stok']);

		//print_r($this->input->post());
		$input['create_by'] = $level['id_unix'];

		// cek nama produk
		$cek_param = (object)array(
            'table' => 'produk',
            'select' => (object)array(
                'string' => "nama",
                'no_quotes' => true
            ),
            'where' => array(       //opsional
                (object) array(
                    'param_name' => 'status_delete',
                    'param_value' => 0,
                ),
                (object) array(
                    'param_name' => 'nama',
                    'param_value' => $input['nama'],
                ),
            ),
			'count' => true
        );

        $res['nama_produk_exist'] = modules::run('crud/crud/get', $cek_param);

		if(!$res['nama_produk_exist']){
			// upload image
			$config['upload_path']="asset/img/produk";
			$config['allowed_types']='gif|jpg|png|jpeg';
			$config['encrypt_name'] = TRUE;
	
			$this->upload->initialize($config);
	
			$res['up_foto'] = $this->upload->do_upload("foto");
	
			if($res['up_foto']) {
				$data = array('upload_data' => $this->upload->data());
				$input['foto'] = $data['upload_data']['file_name'];
	
				$res['insert'] = modules::run('crud/crud/insert', 'produk', $input);
			}
		}

        $this->output->set_content_type('application/json')->set_output(json_encode($res));
	}
	
	public function proses_edit_item()
	{
        //cek leveling baru
        $this->load->library('leveling');
        $this->leveling->cek_level('manage produk');

        $level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));

        $input = $this->input->post();
        unset($input['id']);
        unset($input['stok']);
        $input['update_by'] = $level['id_unix'];

		// cek nama produk
		$cek_param = (object)array(
            'table' => 'produk',
            'select' => (object)array(
                'string' => "nama",
                'no_quotes' => true
            ),
            'where' => array(       //opsional
                (object) array(
                    'param_name' => 'status_delete',
                    'param_value' => 0,
                ),
                (object) array(
                    'param_name' => 'nama',
                    'param_value' => $input['nama'],
                ),
            ),
			'count' => true
        );

        $res['nama_produk_exist'] = modules::run('crud/crud/get', $cek_param);

		if(!$res['nama_produk_exist']){
			// upload image
			$config['upload_path']="asset/img/produk";
			$config['allowed_types']='gif|jpg|png|jpeg';
			$config['encrypt_name'] = TRUE;
	
			$this->upload->initialize($config);
	
			$res['up_foto'] = $this->upload->do_upload("foto");
	
			if($res['up_foto']) {
				$data = array('upload_data' => $this->upload->data());
				$input['foto'] = $data['upload_data']['file_name'];

				$where = array(
					'id' => $this->input->post('id'),
				);
	
				$res['update'] = modules::run('crud/crud/update', 'produk', $where, $input);
			}
		}
        
        // echo $this->db->last_query();
        // echo '<pre>';
        // print_r($this->input->post());
        // echo '</pre>';

        $this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

    public function edit_status(){
        //cek leveling baru
        $this->load->library('leveling');
        $this->leveling->cek_level('manage produk');

        $where = array(
            'id_produk' => $this->input->post('id_produk'),
        );

        $input = array(
            'status' => $this->input->post('status')
        );

        $response = modules::run('crud/crud/update', 'produk', $where, $input);

        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function upload()
    {
        header('Content-Type: application/json'); // set json response headers
        $outData = $this->do_upload(); // a function to upload the bootstrap-fileinput files
        echo json_encode($outData); // return json data
    }

    public function del_imgs()
    {
        $where = array(
            'id_foto_produk' => $this->input->post('key'),
        );
        $res = modules::run('crud/crud/delete', 'foto_produk', $where);
        $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }

    public function do_upload() {
        $preview = $config = $errors = [];
        $targetDir = 'asset/img/produk';
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }
        $identifier = date("YmdH").'_'.$_POST['id_master_produk'].'_'; //biasanya .date("YmdHis") ."_"
        $fileBlob = 'fileBlob';                      // the parameter name that stores the file blob
        if (isset($_FILES[$fileBlob])) {
            $file = $_FILES[$fileBlob]['tmp_name'];  // the path for the uploaded file chunk 
            $fileName = $_POST['fileName'];          // you receive the file name as a separate post data
            $fileSize = $_POST['fileSize'];          // you receive the file size as a separate post data
            $fileId = $_POST['fileId'];              // you receive the file identifier as a separate post data
            $index =  $_POST['chunkIndex'];          // the current file chunk index
            $totalChunks = $_POST['chunkCount'];     // the total number of chunks for this file
            $targetFile = $targetDir.'/'.$identifier.$fileName;  // your target file path

            if ($totalChunks > 1) {                  // create chunk files only if chunks are greater than 1
                $targetFile .= '__' . str_pad($index, 4, '0', STR_PAD_LEFT); 
            } 

            $pure_name = explode("__",$fileName)[0];//ambil filename tanpa ext chunk
            // $thumbnail = 'unknown.jpg';
            if(move_uploaded_file($file, $targetFile)) {
                $outFile = '';
                $upl = false;
                // get list of all chunks uploaded so far to server
                $chunks = glob("{$targetDir}/{$identifier}{$fileName}__*"); 
                // check uploaded chunks so far (do not combine files if only one chunk received)
                $allChunksUploaded = $totalChunks > 1 && count($chunks) == $totalChunks;
                if ($allChunksUploaded) {           // all chunks were uploaded
                    $outFile = $targetDir.'/'.$identifier.$fileName;
                    $ins_db_name = $identifier.$fileName;
                    // combines all file chunks to one file
                    $upl = $this->combineChunks($chunks, $outFile, $ins_db_name);
                } 
                // if you wish to generate a thumbnail image for the file
                // $targetUrl = $this->getThumbnailUrl($targetFile, $fileName);
                $targetUrl = $targetDir.'/'.$identifier.$pure_name;
                // separate link for the full blown image file
                // $zoomUrl = 'http://localhost/uploads/' . $fileName;

                if($totalChunks == 1){
                    // insert to db
                    $data = array(
                        'id_master_produk' => $_POST['id_master_produk'],
                        'nama'      => $identifier.$fileName,
                        // 'path'      => base_url().$targetUrl,
                    );
                    
                    $upl = modules::run('crud/crud/insert', 'foto_produk', $data);
                }

                return [
                    'chunkIndex' => $index,         // the chunk index processed
                    'initialPreview' => base_url().$targetUrl, // the thumbnail preview data (e.g. image)
                    'initialPreviewConfig' => [
                        [
                            'type' => 'image',      // check previewTypes (set it to 'other' if you want no content preview)
                            'caption' => $fileName, // caption
                            'key' => $this->db->insert_id(),       // keys for deleting/reorganizing preview //bisa juga ambil dari last insert id
                            'url' => site_url('manage_produk/del_imgs'),
                            // 'width' => '120px'
                            // 'zoomData' => $zoomUrl, // separate larger zoom data
                        ]
                    ],
                    'append' => true,
                    'to_db' => $upl,
                ];
            } else {
                return [
                    'error' => 'Error uploading chunk ' . $_POST['chunkIndex']
                ];
            }
        }
        return [
            'error' => 'No file found'
        ];
    }
     
    // combine all chunks
    // no exception handling included here - you may wish to incorporate that
    function combineChunks($chunks, $targetFile, $ins_db_name) {
        // open target file handle
        $handle = fopen($targetFile, 'a+');
        
        foreach ($chunks as $file) {
            fwrite($handle, file_get_contents($file));
        }
        
        // you may need to do some checks to see if file 
        // is matching the original (e.g. by comparing file size)
        
        // after all are done delete the chunks
        foreach ($chunks as $file) {
            @unlink($file);
        }
        
        // close the file handle
        fclose($handle);

        // insert to db
        $data = array(
            'id_master_produk' => $_POST['id_master_produk'],
            'nama'      => $ins_db_name,
            // 'path'      => base_url().$targetFile,
        );
        
        return modules::run('crud/crud/insert', 'foto_produk', $data);
    }
     
    // generate and fetch thumbnail for the file
    function getThumbnailUrl($path, $fileName) {
        // assuming this is an image file or video file
        // generate a compressed smaller version of the file
        // here and return the status
        $sourceFile = $path . '/' . $fileName;
        $targetFile = $path . '/thumbs/' . $fileName;
        //
        // generateThumbnail: method to generate thumbnail (not included)
        // using $sourceFile and $targetFile
        //
        if (generateThumbnail($sourceFile, $targetFile) == true) { 
            return 'http://localhost/uploads/thumbs/' . $fileName;
        } else {
            return 'http://localhost/uploads/' . $fileName; // return the original file
        }
    }
}
