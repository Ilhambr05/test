<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20/09/2018
 * Time: 11.08
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_crud extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('Date');
    }

    function login()
    {
        $this->db->from('member');

        $query = $this->db->get();
        return $query;
//		if ($query->num_rows() == 1) {
//			return $query->result();
//		} else {
//			return false;
//		}
    }
    private function _doquery($where){
        $this->db->select('*');
        $this->db->from('brands');
        $this->db->where($where);
        if($_POST['search']['value']){
            $this->db->group_start();
            $this->db->like('description',$_POST['search']['value']);
            $this->db->or_like('created_from_ip',$_POST['search']['value']);
            $this->db->group_end();
        }
        $column_order = array('description','created_from_ip',null);
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        $this->db->order_by("id", "desc");
    }
    function list_app($where)
    {
        $this->_doquery($where);
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query=$this->db->get();
        return $query->result();
    }
    function count_filtered($where)
    {

        $this->_doquery($where);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all($where)
    {
//		$this->db->select('*');
        $this->db->from('brands');
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    function get($param)
    {
        //select field
        if (isset($param->select)) {
            if (is_object($param->select)) {
                if(isset($param->select->no_quotes)){
                    $this->db->select($param->select->string,false);
                }
            }else{
                $this->db->select($param->select);
            }
        }

        //where
        if (isset($param->where)) {
            foreach ($param->where as $w) {
                if(isset($w->one_string)){
                    $this->db->where($w->string, null,false);
                }else{
                    $this->db->where($w->param_name, $w->param_value);
                }
            }
        }

        //join
        if (isset($param->join)) {
            foreach ($param->join as $j) {
                if (isset($j->align)) {
                    $this->db->join($j->join_table, $j->on, $j->align);
                } else {
                    $this->db->join($j->join_table, $j->on);
                }
                if (isset($j->deleted_hide)) {
                    $this->db->where($j->join_table . ".deleted_at", null);
                }
            }
        }

        //limit
        if (isset($param->limit)) {
            if(isset($param->limit->count) || isset($param->limit->index)){
                $this->db->limit($param->limit->count,$param->limit->index);
            } else {
                $this->db->limit($param->limit);
            }
        }

        //order by
        if (isset($param->order_by)) {
            $this->db->order_by($param->order_by);
        }

        //not deleted
        if (isset($param->deleted)) {
            if ($param->deleted == true) {
                $this->db->where($param->table . ".status_delete", 1);
            }
            else {
                $this->db->where($param->table . ".status_delete", 0);
            }
        }

        //group by
        if (isset($param->group_by)) {
            $this->db->group_by($param->group_by);
        }

        //table select
        $this->db->from($param->table);

        //result count
        if(isset($param->count)){
            return $this->db->count_all_results();
        }

        //result count
        if(isset($param->count)){

            return $this->db->count_all_results();
        }

        //result row
        if(isset($param->row)){
            return $this->db->get()->row();
        }else{
            //build query and result
            $data = $this->db->get()->result();
        }

        //result debug
        if(isset($param->debug)){
            echo $this->db->last_query();
        }

        //child
        if (isset($param->child)) {
            foreach ($data as $d) {
                foreach ($param->child as $c) {
                    $child = new Model_crudapi();
                    //where relation
                    $where_relation = (object)array(
                        "param_name"=>$c->foreignkey,
                        "param_value"=>isset($d->{$c->foreignkey})?$d->{$c->foreignkey}:$d->id
                    );
                    if (isset($c->param)) {
                        $param_child = $c->param;
                        if (isset($param_child->where)) {
                            $param_child->where[] = $where_relation;
                        } else {
                            $param_child->where = array($where_relation);
                        }
                    } else {
                        $param_child = (object)array(
                            "where"=>array($where_relation)
                        );
                    }

                    $child->set_data((object)array(
                        "table_name"=>$c->table_name
                    ));
                    $d->{$c->table_name} = $child->get($param_child);//$this->db->get($c->table_name)->result();
                    unset($param_child->where);
                }
            }
        }

        return $data;
    }

    function insert($tableName, $data)
    {
        $level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));

        $data['date_created'] = $this->date->getDateFull();//tambah data date_created ke input post
		if(!$data['create_by'] && isset($level['id_unix'])){
			$data['create_by'] = $level['id_unix'];
		}

        $res = $this->db->insert($tableName,$data);
        return $res;
    }

    function edit($param)
    {
        if (isset($param->select)) {
            if (is_object($param->select)) {
                if (isset($param->select->no_quotes)) {
                    $this->db->select($param->select->string, false);
                }
            } else {
                $this->db->select($param->select);
            }
        }

        //tableName
        $this->db->from($param->table);

        //  where
        if (isset($param->where)) {
            foreach ($param->where as $w) {
                if (isset($w->one_string)) {
                    $this->db->where($w->string, null, false);
                } else {
                    $this->db->where($w->param_name, $w->param_value);
                }
            }
        }

        $query = $this->db->get();
        return $query->result();
    }
    function update($tabelName, $where, $data)
    {
        $level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));
        
        $data['date_updated'] = $this->date->getDateFull();
        // $data['update_by'] = $level['id_unix'];
        $this->db->where($where);
        $this->db->update($tabelName, $data);
        return $this->db->affected_rows() > 0;
    }
    function update_escape($tabelName, $where, $column, $operator, $mod)
    {
        $inc = $column.' '.$operator.' '.$mod;
        $this->db->set('date_updated', $this->date->getDateFull());
        $this->db->set($column, $inc, false);
        $this->db->where($where);
        $this->db->update($tabelName);
        return $this->db->affected_rows() > 0;
    }
    function delete($tabelName,$where)
    {
        $level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));
        $data = array(
            'status_delete' => 1,
            'delete_from' =>  $level['id_unix'],
            'date_updated' => $this->date->getDateFull()
        );
        $this->db->where($where);
        $this->db->update($tabelName, $data);
        return $this->db->affected_rows() > 0;
    }
}

?>
