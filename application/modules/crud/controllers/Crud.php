<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CRUD extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->model('Model_crud', 'model');
    }
    function get($param)
    {
        return $this->model->get($param);
    }
    function insert($nama_tabel, $data)
    {
        return $this->model->insert($nama_tabel, $data);
    }
    function edit($param)
    {
        return $this->model->edit($param);
    }
    function update($tabelName, $where, $data)
    {
        return $this->model->update($tabelName, $where, $data);
    }
    function update_escape($tabelName, $where, $col, $op, $mod)
    {
        return $this->model->update_escape($tabelName, $where, $col, $op, $mod);
    }
    function delete($tabelName, $where)
    {
        return $this->model->delete($tabelName, $where);
    }
}
?>