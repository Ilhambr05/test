<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_login extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function login($username, $password)
	{
		$this->db->select('admin.*');
		$this->db->from('admin');
		$this->db->where('username', $username);
		$this->db->where('password', md5($password));
		$this->db->where('admin.status_delete', 0);
		// $this->db->where('activated', 1);
		$this->db->limit(1);

		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}

	function cek_pass($password)
	{
		$data_session = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));

		$this->db->from('admin');
		$this->db->where('id_admin', $data_session['id']);
		$this->db->where('password', md5($password));
		$this->db->limit(1);

		return $this->db->get()->num_rows();
	}
}
