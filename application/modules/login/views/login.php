<?php $this->load->view('admin_template/head.php')?>

<body class="off-canvas-sidebar">
  <div class="wrapper wrapper-full-page">
    <div class="page-header login-page header-filter" filter-color="black" 
    style="background-image: url('<?php echo base_url('asset/admin_material/img/cover2.jpg'); ?>'); 
    background-size: cover; background-position: top center;">
      <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
              <div class="card card-login">
                <div class="card-header card-header-success text-center">
                  <h4 class="card-title"><?php echo getenv('NAME_PROJECT') ?></h4>
                </div>
                <form method="post" action="<?php echo base_url(); ?>login" role="login">
                <div class="card-body pr-4 pl-4">
                    <?php if(!validation_errors()){echo '<p></p><p class="card-description text-center">Masukkan Username / Password Anda</p><p></p>';}else{echo validation_errors();} ?>
                    <span class="bmd-form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text pl-0">
                            <i class="material-icons">face</i>
                          </span>
                        </div>
                        <input name="username" type="input" class="form-control" placeholder="Username" autofocus required>
                      </div>
                    </span>
                    <span class="bmd-form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text pl-0">
                            <i class="material-icons">lock_outline</i>
                          </span>
                        </div>
                        <input name="password" type="password" class="form-control" placeholder="Password" required>
                      </div>
                    </span>
                    <br>
                    <p></p>
                </div>
                <div class="card-footer justify-content-center">
                  <button type="submit" class="btn btn-primary btn-lg">Log In</button>
                </div>
                </form>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
