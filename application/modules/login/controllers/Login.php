<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Model_login');
		$this->load->helper("URL", "DATE", "URI", "FORM");
		$this->load->library('javascript');
		$this->load->helper('security');
		$this->load->library('session');
	}

	function index()
	{
		$this->load->library('form_validation');
		$this->form_validation->CI = &$this;

		$this->form_validation->set_rules('username', 'username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean|callback_check_database');

		if ($this->form_validation->run($this) == FALSE) {
			$data_session = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));
			if ($data_session['nama_user']) {
				redirect('dashboard');
			} else {
				$this->load->helper(array('form'));
				$this->load->view('login');
			}
		} else {
			$data_session = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));
			if ($data_session['nama_user']) {
				redirect('dashboard');
			} else {
				$this->load->helper(array('form'));
				$this->load->view('login');
			}
		}
	}
	public function logout()
	{
		$this->session->unset_userdata(getenv("NAME_SESSION_DASHBOARD"));
		// session_destroy();
		redirect('login');
	}
	function check_database($password)
	{
		$username = $this->input->post('username');
		$result = $this->Model_login->login($username, $password);

		if ($result) {
			$sess_array = array();
			foreach ($result as $row) {
				$level = 'admin';
				if (!$row->status_admin) {
					//status = 0 = super admin
					$level = 'super admin';
				}
				$sess_array = array(
					'id_unix' => $row->id,
					'nama_user' => $row->nama_admin,
					'level'		=> $level,
					'leveling'	=> $this->get_list_level($row->list_menu),
				);
				$this->session->set_userdata(getenv("NAME_SESSION_DASHBOARD"), $sess_array);
			}
			return TRUE;
		} else {
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			return false;
		}
	}

	private function get_list_level($leveling)
	{
		$cek_param = (object) array(
			'table' => 'menu',
			'select' => (object)array(
				'string' => "nama",
				'no_quotes' => true
			),
			'where' => array(       //opsional
				(object) array(
					'one_string' => true,
					'string' => 'id_menu in (' . $leveling . ')',
				),
			),
		);
		$res = modules::run('crud/crud/get', $cek_param);

		$arr = array();
		foreach ($res as $row) {
			$arr[] = $row->nama;
		}

		return $arr;
	}
}
