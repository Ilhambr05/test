<?php $this->load->view('admin_template/head.php')?>
<?php $level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD")); ?>

	 <link href="<?php echo base_url('asset/admin_material/select2/select2.min.css'); ?>" rel="stylesheet" />

	<link href="<?php echo base_url('asset/file-upload/css/fileinput.css') ?>" media="all" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
	<link href="<?php echo base_url('asset/file-upload/themes/explorer-fas/theme.css') ?>" media="all" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet"href="<?php echo base_url('asset/admin_material/daterangepicker/daterangepicker.css') ?>">
	<body >
		<div class="wrapper ">
			<?php $this->load->view('admin_template/sidebar-material.php')?>
			<div class="main-panel">
				<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
					<div class="container-fluid">
						<div class="navbar-wrapper">
							<a class="navbar-brand" href="#pablo">Manage Pemesanan</a>
						</div>
					</div>
				</nav>
				<!-- End Navbar -->
				<div class="content">
					<section class="content">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-12">
									<div class="card">
										<div class="card-header card-header-primary">
											<h4 class="card-title ">List Pemesanan
											</h4>
										</div>
										<div class="card-body">
											<div class="row">
												<div class="col-md-3 pr-0">
													<input type="text" class="form-control" id="filter_tgl"/>
												</div>
												<button class="btn btn-success btn-round col-md-2 m-0" onclick="show_data()">
													Filter
												</button>
											</div>
											<div class="toolbar"></div>
											<div class="material-datatables">
												<table class="table" id="example1">
													<thead>
													<tr>
														<th>Kode pemesanan</th>
														<th>Nama Customer</th>
														<th>List Pesanan</th>
														<th>Grand Total</th>
														<th>No HP - Email</th>
														<th>Waktu Pesan</th>
													</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
		</div>

	</body>
<!--   Core JS Files   -->
<?php $this->load->view('admin_template/js.php')?>

<!--tambahkan custom js disini-->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url('mix_assets/js/manage_pemesanan_'.getenv("MANAGE_PEMESANAN").'.js') ?>"></script>
<script src="<?php echo base_url('asset/admin_material/daterangepicker/daterangepicker.js') ?>"></script>
<script>
	var startDate = '<?php echo date("Y-m-d"); ?>'
	var endDate = '<?php echo date("Y-m-d"); ?>';
	$('#filter_tgl').daterangepicker({
		locale: {
			format: 'DD-MM-YYYY'
		},
		startDate: '<?php echo date('d-m-Y'); ?>',
		endDate:  '<?php echo date('d-m-Y'); ?>'
	}, function(start, end, label) {
		startDate = start.format('YYYY-MM-DD');
		endDate = end.format('YYYY-MM-DD');
	});
</script>
<?php
$this->load->view('admin_template/foot');
?>
