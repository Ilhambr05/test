<?php
defined('BASEPATH') or exit('No direct script access allowed');

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;

class manage_pemesanan extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper("URL", "DATE", "URI", "FORM");
		$this->load->library('Date');
		//cek leveling baru
		$this->load->library('leveling');
		$this->leveling->cek_level('manage pemesanan');
	}

	public function index()
	{
		$this->load->view('index');
	}

	public function ajax_list($start, $end)
	{
		$level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));

		$where = array( //opsional
			(object) array(
				'param_name' => 'status_delete',
				'param_value' => 0,
			)
		);

		if ($start != 0 and $end != 0) {
			$where[] = (object) array(
				'one_string' => 'true',
				'string' => 'date(date_created) >= "' . $start . '" and date(date_created) <= "' . $end . '"',
			);
		}

		$value = (object) array(
			'table' => 'pemesanan', //wajib
			'select' => (object) array( //wajib
				'string' => "id,pemesanan.kode_pesan, atas_nama, pemesanan.date_created,  pemesanan.no_hp, pemesanan.email, pemesanan.grand_total",
				'no_quotes' => true,
			),
			'where' => $where,
			'columnSearch' => array('pemesanan.kode_pesan', 'atas_nama', 'grand_total', 'no_hp', 'email', "date_created"),
			'columnOrder' => array('pemesanan.kode_pesan', 'atas_nama', 'grand_total', 'no_hp', 'email', 'pemesanan.date_created', null), //wajib
			'order_by' => "pemesanan.date_created desc", //wajib
		);

		$data = modules::run('datatables/datatables/datatablesData', $value);
		$list = $data;
		$data = array();
		foreach ($list as $lihat) {
			$row = array();
			$row[] = $lihat->kode_pesan;
			$row[] = $lihat->atas_nama;
			$row[] = $this->get_list_pesanan($lihat->id);
			$row[] = $this->rupiah($lihat->grand_total);
			$row[] = $lihat->no_hp."<br>".$lihat->email;
			$tgl = explode(' ', $lihat->date_created);
			$row[] = $this->date->tanggal_indo($tgl[0]);

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => modules::run('datatables/datatables/countAll', $value),
			"recordsFiltered" => modules::run('datatables/datatables/countFiltered', $value),
			"data" => $data,
		);
		//output to json format
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
	private function rupiah($angka)
	{

		$hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
		return $hasil_rupiah;
	}

	private function get_list_pesanan($id_pemesanan)
	{
		$value = (object) array(
			'table' => 'detail_pemesanan', //wajib
			'select' => (object) array( //wajib
				'string' => "id, nama_item, jml, harga",
				'no_quotes' => true,
			),
			'where' => array( //opsional
				(object) array(
					'param_name' => 'id_pemesanan',
					'param_value' => $id_pemesanan,
				),
				(object) array(
					'param_name' => 'status_delete',
					'param_value' => 0,
				),
			),
		);

		$detail = modules::run('crud/crud/get', $value);

		$table = '<table border=0><tbody>';
		foreach ($detail as $key => $value) {
			$table .= "<tr>";
			$table .= "<td>".$value->nama_item."</td>";
			$table .= "<td class='text-right'>X ".$value->jml."</td>";
			$table .= "<td class='text-right'>@ ". number_format($value->harga, 2, ',', '.')."</td>";
			$table .= "</tr>";
		}
		$table .= "</tbody>";

		return $table;
	}
}
