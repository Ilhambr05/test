<?php $this->load->view('admin_template/head.php') ?>
<?php $level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD")); ?>
<link href="<?php echo base_url('asset/file-upload/css/fileinput.css') ?>" media="all" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
<link href="<?php echo base_url('asset/file-upload/themes/explorer-fas/theme.css') ?>" media="all" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<body>
	<div class="wrapper ">
		<?php $this->load->view('admin_template/sidebar-material.php') ?>
		<div class="main-panel">
			<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
				<div class="container-fluid">
					<div class="navbar-wrapper">
						<a class="navbar-brand" href="#pablo">Dashboard</a>
					</div>
				</div>
			</nav>
			<!-- End Navbar -->
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header card-header-warning card-header-icon">
									<div class="card-icon">
										<i class="material-icons">shopping_cart</i>
									</div>
									<p class="card-category">Pesanan Masuk</p>
									<h4 class="card-title"><span id="jumlah_pemesanan"><?php echo $jumlah_pemesanan; ?></span>
									</h4>
								</div>
								<div class="card-footer">
									<!--								<div class="stats">-->
									<!--									<i class="material-icons text-danger">warning</i>-->
									<!--									<a href="javascript:;">Get More Space...</a>-->
									<!--								</div>-->
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header card-header-info card-header-icon">
									<div class="card-icon">
										<i class="material-icons">all_inbox</i>
									</div>
									<p class="card-category">Produk Terdaftar</p>
									<h4 class="card-title">
										<strong><span id="jumlah_produk"><?php echo $jumlah_produk; ?></span></strong>
									</h4>
								</div>
								<div class="card-footer">
									<!--								<div class="stats">-->
									<!--									<i class="material-icons text-danger">warning</i>-->
									<!--									<a href="javascript:;">Get More Space...</a>-->
									<!--								</div>-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
<!--   Core JS Files   -->
<?php $this->load->view('admin_template/js.php') ?>

<!--tambahkan custom js disini-->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php
$this->load->view('admin_template/foot');
?>
