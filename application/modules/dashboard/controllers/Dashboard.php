<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Dashboard extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper("URL", "DATE", "URI", "FORM");
		$this->load->library('session');
		$this->load->library('Date');
		//cek leveling baru
		$this->load->library('leveling');
	}

	public function index()
	{
		$this->leveling->cek_level('dashboard');
		$data['jumlah_pemesanan'] = $this->get_jumlah_pesanan();
		$data['jumlah_produk'] = $this->get_jumlah_produk();

		$this->load->view('dashboard', $data);
	}

	public function get_jumlah_produk()
	{
		$value = (object) array(
			'table' => 'produk', //wajib
			'select' => (object) array( //wajib
				'string' => "id",
				'no_quotes' => true,
			),
			'where' => array( //opsional
				(object) array(
					'param_name' => 'status_delete',
					'param_value' => 0,
				),
			),
			'count' => true
		);

		return modules::run('crud/crud/get', $value);
	}

	public function get_jumlah_pesanan()
	{
		$value = (object) array(
			'table' => 'pemesanan', //wajib
			'select' => (object) array( //wajib
				'string' => "id, nama_item, jml, harga",
				'no_quotes' => true,
			),
			'where' => array( //opsional
				(object) array(
					'param_name' => 'status_delete',
					'param_value' => 0,
				),
			),
			'count' => true
		);

		return modules::run('crud/crud/get', $value);
	}
}
