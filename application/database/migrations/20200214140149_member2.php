<?php

class Migration_member2 extends CI_Migration {

    public function up() {
        $this->db->query('ALTER TABLE `member` ADD `no_hp` INT(15) NOT NULL AFTER `nama`;');
    }

    public function down() {
        $this->db->query('ALTER TABLE `member` DROP `no_hp`;');
    }

}