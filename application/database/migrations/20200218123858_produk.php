<?php

class Migration_produk extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id_produk' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
			'id_master_produk' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'id_lokasi' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'stok' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'status' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'create_by' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'update_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'delete_from' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'status_delete' => array(
				'type' => 'INT',
				'constraint' => 1,
				'default' => 0,
				'comment' => '0 untuk tidak didelete , 1 sudah didelete'
			),
			'date_created' => array(
				'type' => 'DATETIME',
				'null' => TRUE
			),
			'date_updated' => array(
				'type' => 'DATETIME',
				'null' => TRUE
			),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('produk');
		$this->db->query(add_foreign_key('produk', 'id_master_produk', 'master_produk(id_master_produk)', 'CASCADE', 'CASCADE'));
		$this->db->query(add_foreign_key('produk', 'id_lokasi', 'lokasi(id_lokasi)', 'CASCADE', 'CASCADE'));
    }

    public function down() {
        $this->dbforge->drop_table('produk');
		$this->db->query(drop_foreign_key('produk','id_master_produk'));
		$this->db->query(drop_foreign_key('produk','id_lokasi'));
    }

}
