<?php

class Migration_member extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id_member' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => FALSE
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => FALSE
			),
			'no_hp' => array(
				'type' => 'VARCHAR',
				'constraint' => 15,
				'null' => TRUE
			),
			'password' => array(
				'type' => 'TEXT',
				'null' => TRUE
			),
			'hascode' => array(
				'type' => 'TEXT',
				'null' => TRUE
			),
			'provider' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE
			),
			'provider_id' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE
			),
			'fcm' => array(
				'type' => 'TEXT',
				'null' => TRUE
			),
			'image_url' => array(
				'type' => 'TEXT',
				'null' => TRUE
			),
			'player_id' => array(
				'type' => 'TEXT',
				'null' => TRUE
			),
			'last_login' => array(
				'type' => 'ENUM("WEB","ANDROID")',
				'null' => FALSE,

			),
			'activated' => array(
				'type' => 'INT',
				'constraint' => 1,
				'default' => 0,
				'comment' => '0 untuk tidak aktif , 1 aktif'
			),
			'point' => array(
				'type' => 'double',
				'default' => 0
			),
			'create_by' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'update_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'delete_from' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'status_delete' => array(
				'type' => 'INT',
				'constraint' => 1,
				'default' => 0,
				'comment' => '0 untuk tidak didelete , 1 sudah didelete'
			),
			'date_created' => array(
				'type' => 'DATETIME',
				'null' => TRUE
			),
			'date_updated' => array(
				'type' => 'DATETIME',
				'null' => TRUE
			),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('member');
    }

    public function down() {
        $this->dbforge->drop_table('member');
    }

}
