<?php

class Migration_admin extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id_admin' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'id_karyawan' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'id_lokasi' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => FALSE
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => FALSE
            ),
            'status_admin' => array(
                'type' => 'INT',
                'constraint' => 2,
                'default' => 1
            ),
            'list_menu' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'create_by' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'update_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => 0
            ),
            'delete_from' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => 0
            ),
            'status_delete' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0,
				'comment' => '0 untuk tidak didelete , 1 sudah didelete'
            ),
            'date_created' => array(
                'type' => 'DATETIME',
                'null' => TRUE
            ),
            'date_updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('admin');
		$this->db->query(add_foreign_key('admin', 'id_karyawan', 'karyawan(id_karyawan)', 'CASCADE', 'CASCADE'));
    }

    public function down() {
        $this->dbforge->drop_table('admin');
        $this->db->query(drop_foreign_key('admin','id_karyawan'));
    }

}
