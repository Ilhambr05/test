<?php

class Migration_master_produk extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id_master_produk' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
			'id_master_jenis' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => FALSE,
			),
			'satuan' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE,
			),
			'deskripsi' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'harga' => array(
				'type' => 'DOUBLE',
				'default' => 0,
			),

			'create_by' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'update_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'delete_from' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'status_delete' => array(
				'type' => 'INT',
				'constraint' => 1,
				'default' => 0,
				'comment' => '0 untuk tidak didelete , 1 sudah didelete'
			),
			'date_created' => array(
				'type' => 'DATETIME',
				'null' => TRUE
			),
			'date_updated' => array(
				'type' => 'DATETIME',
				'null' => TRUE
			),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('master_produk');
		$this->db->query(add_foreign_key('master_produk', 'id_master_jenis', 'master_jenis(id_master_jenis)', 'CASCADE', 'CASCADE'));
    }

    public function down() {
        $this->dbforge->drop_table('master_produk');
		$this->db->query(drop_foreign_key('master_produk','id_master_jenis'));
    }

}
