<?php

class Migration_master_jenis extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id_master_jenis' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => FALSE,
			),
			'keterangan' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'create_by' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'update_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'delete_from' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'status_delete' => array(
				'type' => 'INT',
				'constraint' => 1,
				'default' => 0,
				'comment' => '0 untuk tidak didelete , 1 sudah didelete'
			),
			'date_created' => array(
				'type' => 'DATETIME',
				'null' => TRUE
			),
			'date_updated' => array(
				'type' => 'DATETIME',
				'null' => TRUE
			),

        ));
        $this->dbforge->add_key('id_master_jenis', TRUE);
        $this->dbforge->create_table('master_jenis');
    }

    public function down() {
        $this->dbforge->drop_table('master_jenis');
    }

}
