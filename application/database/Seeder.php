<?php
/**
 * Created by PhpStorm.
 * User: Yoka
 * Date: 05/09/18
 * Time: 11.25
 */

class Seeder
{
    private $CI;
    protected $db;
    protected $dbforge;
    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->database();
        $this->CI->load->dbforge();
        $this->db = $this->CI->db;
        $this->dbforge = $this->CI->dbforge;
    }
    /**
     * Run another seeder
     *
     * @param string $seeder Seeder classname
     */
    public function call($seeder)
    {
        $file = APPPATH . 'database/seeds/' . $seeder . '.php';
        require_once $file;
        $obj = new $seeder;
        $obj->run();
    }
    public function __get($property)
    {
        return $this->CI->$property;
    }
}

/*
 *---------------------------------------------------------------
 * EXAMPLE MIGRATIONS AND SEEDER
 *---------------------------------------------------------------
 *
 * CLI MIGRATION
 * php index.php tools migration Brands
 * php index.php tools migrate
 *
 * CLI SEEDER
 * php index.php tools seeder BrandsSeeder
 * php index.php tools seed BrandsSeeder
*/