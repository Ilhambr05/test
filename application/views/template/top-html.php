<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url('asset/material/img/favicon.png');?>">
<link rel="icon" type="image/png" href="<?php echo site_url('asset/material/img/favicon.png');?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>
  Majoo Teknologi Indonesia
</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

<!-- <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,600,600i,700|Source+Sans+Pro:300,400,400i,600,600i,700,700i" rel="stylesheet"> -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<!-- <link href="<?php echo site_url('impressions_asset/css/css.css'); ?>" rel="stylesheet">  -->
<link rel="stylesheet" href="<?php echo site_url('impressions_asset/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" href="<?php echo site_url('impressions_asset/css/animate.css'); ?>">
<link rel="stylesheet" href="<?php echo site_url('impressions_asset/css/font-awesome.min.css'); ?>">
<link rel="stylesheet" href="<?php echo site_url('impressions_asset/css/style.css'); ?>">
<link rel="stylesheet" href="<?php echo site_url('asset/select2/select2.min.css'); ?>">

<!-- font style -->
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="favicons/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<link href="<?php echo site_url('mix_assets/css/style_ori_'.getenv("STYLE_ORI").'.css'); ?>" rel="stylesheet">
<link href="<?php echo site_url('mix_assets/css/style-material_'.getenv("STYLE_MATERIAL").'.css'); ?>" rel="stylesheet">
<style type="text/css">
  .g-signin2{
    /*width: 100%;*/
  }

  .g-signin2 > div{
    width: 100% !important;
    border-radius: 5px;
    margin-bottom: 10px;
  }

  /* Shared */
  .loginBtn {
    width: 100%;
    box-sizing: border-box;
    position: relative;
    /* width: 13em;  - apply for fixed size */
    /*margin: 4px 1px;*/
    padding: 0 0 0 30px;
    border: none;
    text-align: center;
    line-height: 37px;
    white-space: nowrap;
    border-radius: 0.4285rem;
    font-size: 0.75rem;
    color: #FFF;
  }
  .loginBtn:before {
    content: "";
    /*box-sizing: border-box;*/
    position: absolute;
    top: 0;
    left: 0;
    width: 36px;
    height: 100%;
  }
  .loginBtn:focus {
    outline: none;
  }
  .loginBtn:active {
    box-shadow: inset 0 0 0 32px rgba(0,0,0,0.1);
  }


  /* Facebook */
  .loginBtn--facebook {
    background-color: #4C69BA;
    background-image: linear-gradient(#4C69BA, #3B55A0);
    /*font-family: "Helvetica neue", Helvetica Neue, Helvetica, Arial, sans-serif;*/
    text-shadow: 0 -1px 0 #354C8C;
  }
  .loginBtn--facebook:before {
    border-right: #364e92 1px solid;
    background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png') 6px 6px no-repeat;
  }
  .loginBtn--facebook:hover,
  .loginBtn--facebook:focus {
    background-color: #5B7BD5;
    background-image: linear-gradient(#5B7BD5, #4864B1);
  }


  /* Google */
  .loginBtn--google {
    /*font-family: "Roboto", Roboto, arial, sans-serif;*/
    background: #DD4B39;
  }
  .loginBtn--google:before {
    border-right: #BB3F30 1px solid;
    background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_google.png') 6px 6px no-repeat;
  }
  .loginBtn--google:hover,
  .loginBtn--google:focus {
    background: #E74B37;
  }
  .row:before, .row:after {display: none !important;}

  .bg-svg3{
    background-color: #262a39;
    background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='464' height='464' viewBox='0 0 800 800'%3E%3Cg fill='none' stroke='%233e4144' stroke-width='1'%3E%3Cpath d='M769 229L1037 260.9M927 880L731 737 520 660 309 538 40 599 295 764 126.5 879.5 40 599-197 493 102 382-31 229 126.5 79.5-69-63'/%3E%3Cpath d='M-31 229L237 261 390 382 603 493 308.5 537.5 101.5 381.5M370 905L295 764'/%3E%3Cpath d='M520 660L578 842 731 737 840 599 603 493 520 660 295 764 309 538 390 382 539 269 769 229 577.5 41.5 370 105 295 -36 126.5 79.5 237 261 102 382 40 599 -69 737 127 880'/%3E%3Cpath d='M520-140L578.5 42.5 731-63M603 493L539 269 237 261 370 105M902 382L539 269M390 382L102 382'/%3E%3Cpath d='M-222 42L126.5 79.5 370 105 539 269 577.5 41.5 927 80 769 229 902 382 603 493 731 737M295-36L577.5 41.5M578 842L295 764M40-201L127 80M102 382L-261 269'/%3E%3C/g%3E%3Cg fill='%23094755'%3E%3Ccircle cx='769' cy='229' r='5'/%3E%3Ccircle cx='539' cy='269' r='5'/%3E%3Ccircle cx='603' cy='493' r='5'/%3E%3Ccircle cx='731' cy='737' r='5'/%3E%3Ccircle cx='520' cy='660' r='5'/%3E%3Ccircle cx='309' cy='538' r='5'/%3E%3Ccircle cx='295' cy='764' r='5'/%3E%3Ccircle cx='40' cy='599' r='5'/%3E%3Ccircle cx='102' cy='382' r='5'/%3E%3Ccircle cx='127' cy='80' r='5'/%3E%3Ccircle cx='370' cy='105' r='5'/%3E%3Ccircle cx='578' cy='42' r='5'/%3E%3Ccircle cx='237' cy='261' r='5'/%3E%3Ccircle cx='390' cy='382' r='5'/%3E%3C/g%3E%3C/svg%3E");
    background-attachment: fixed;
  }
</style>
