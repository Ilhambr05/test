<section id="footer" style="border-top: 1px solid #eee;">

  <div class="container text-center text-md-left pt-5">
		<div class="copyright-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
						<p class="copyrights">Copyright <i class="icofont icofont-copyright"></i> 2019. PT. Majoo Teknologi Indonesia.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

