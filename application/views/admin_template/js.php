<script src="<?php echo base_url('asset/admin_material/js/core/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/admin_material/js/core/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/admin_material/js/core/bootstrap-material-design.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/admin_material/js/plugins/perfect-scrollbar.jquery.min.js'); ?>"></script>
<!-- Plugin for the momentJs  -->
<script src="<?php echo base_url('asset/admin_material/js/plugins/moment.min.js'); ?>"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="<?php echo base_url('asset/admin_material/js/plugins/bootstrap-datetimepicker.min.js'); ?>"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="<?php echo base_url('asset/admin_material/js/plugins/jquery.dataTables.min.js'); ?>"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="<?php echo base_url('asset/admin_material/js/plugins/nouislider.min.js'); ?>"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?php echo base_url('asset/admin_material/js/material-dashboard.js?v=2.1.1'); ?>" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url('asset/admin_material/demo/demo.js'); ?>"></script>