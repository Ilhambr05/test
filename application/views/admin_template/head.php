<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url("asset/material/img/favicon.png"); ?>">
  <link rel="icon" type="image/png" href="<?php echo site_url("asset/material/img/favicon.png"); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
      <?php echo getenv('NAME_PROJECT') ?>
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?php echo base_url('asset/admin_material/css/material-dashboard.css?v=2.1.1'); ?>" rel="stylesheet" />
  <link href="<?php echo base_url('asset/admin_material/css/material-dashboard-pro.css?v=2.1.1'); ?>" rel="stylesheet" />
  <!-- <link href="https://demos.creative-tim.com/material-dashboard-pro/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet" /> -->

    <style>
        .sidebar .nav li a{
          margin: 5px 15px 0;
        }
        form .form-group select.form-control{
            position: relative;
        }
        /*penyelarasan Style datatables*/
        table{
          width: 100% !important;
        }
        /*.dataTables_paginate, .dataTables_filter{
            float: right;
        }
        .dataTables_filter {
            width: 50%;
        }
        .dataTables_filter label{
            width: 100%;
        }
        .sidebar .material-icons, .fa{
          margin-left: -5px;
        }*/
        /*Ilham custom*/
        .modal .control-label{
          position: unset;
          font-weight: 300;
        }
        #map{
          margin-top: 0px !important;
        }
        .nav .notification {
            z-index: 1;
          position: absolute;
          margin-top: -5px;
          margin-left: 15px;
          border: 1px solid #FFF;
          font-size: 9px;
          background: #f44336;
          color: #FFFFFF;
          min-width: 20px;
          padding: 0px 5px;
          height: 20px;
          border-radius: 10px;
          text-align: center;
          line-height: 19px;
          vertical-align: middle;
          display: block;
        }

        .collapse .nav, .nav-item{
          margin-top: 3px;
        }

        /*penyesuaian daterangepicker di tema*/
        .daterangepicker.dropdown-menu {
            transform: none !important;
            transform-origin: unset;
            opacity: 1 !important;
            font-size: 12px !important;
        }

        .label-custom{
          position: unset !important;
        }
    </style>
</head>