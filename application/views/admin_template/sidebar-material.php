<div class="sidebar" data-color="purple" data-background-color="black"
     data-image="<?php echo base_url('asset/admin_material/img/sidebar-1.jpg'); ?>">
      <?php 
        $level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));
        $leveling = [];
        if($level['leveling']){
            $leveling = $level['leveling'];
        }
        $this->load->library('leveling');
      ?>
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
      -->
      <div class="logo">
        <!-- <a href="" class="simple-text logo-mini">
            <?php echo getenv('NAME_PROJECT_ALIAS') ?>
        </a> -->
        <a href="" class="simple-text logo-normal">
            <?php echo getenv('NAME_PROJECT') ?>
        </a>
      </div>
      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo bg-white">
            <p style="text-align: center; padding: 5px;"><?php if($level['level'] == 'super admin'){echo 'SA';}else{echo "Ad";} ?></p>
          </div>
          <div class="user-info">
            <a data-toggle="collapse" href="#collapseExample" class="username collapsed" aria-expanded="false">
              <span style="text-overflow:ellipsis; overflow:hidden; white-space:nowrap; padding-right:25px;">
                <?php echo $level['nama_user']; ?>
                <b class="caret"></b>
              </span>
            </a>
            <div class="collapse w-100" id="collapseExample" style="">
              <div style="margin-bottom: 10px;"></div>
              <ul class="nav">
                <li class="nav-item <?php if($this->uri->segment(2)=="profile"){echo "active";}?>">
                  <a href="<?php echo site_url('profile'); ?>" class="nav-link">
                    <span class="sidebar-mini"> Pr </span>
                    <span class="sidebar-normal"> Profile </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo base_url(); ?>login/logout" class="nav-link bg-danger">
                    <span class="sidebar-mini"> <i class="material-icons">exit_to_app</i> </span>
                    <span class="sidebar-normal"> Log Out </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <ul class="nav">
			<!--	TODO MENU DASHBOARD	-->
			<?php if(in_array('dashboard', $leveling)){ //sesuaikan leveling dg list_menu d db ?>
				<li class="nav-item <?php if($this->uri->segment(1)=="dashboard"){echo "active";}?>">
					<a class="nav-link" href="<?php echo base_url(); ?>dashboard">
						<i class="material-icons">dashboard</i>
						<p>Dashboard</p>
					</a>
				</li>
			<?php } ?>
			<!--	TODO MENU MANAGE PEMESANAN		-->
			<?php if(in_array('manage pemesanan', $leveling)){ //sesuaikan leveling dg list_menu d db ?>
				<li class="nav-item <?php if($this->uri->segment(1)=="manage_pemesanan"){echo "active";}?>">
					<a class="nav-link" href="<?php echo base_url(); ?>manage_pemesanan">
						<i class="material-icons">shopping_cart</i>
						<p>Manage Pemesanan</p>
					</a>
				</li>
			<?php } ?>
			<!--	MENU MANAGE PRODUK	-->
			<?php if(in_array('manage produk', $leveling)){ //sesuaikan leveling dg list_menu d db ?>
				<li class="nav-item <?php if($this->uri->segment(1)=="manage_produk"){echo "active";}?>">
					<a class="nav-link" href="<?php echo base_url(); ?>manage_produk">
						<i class="material-icons">all_inbox</i>
						<p>Manage Produk</p>
					</a>
				</li>
			<?php } ?>
        </ul>
      </div>
    </div>
