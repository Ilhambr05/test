	<script src="<?php echo base_url('mix_assets/js/notify.min.js') ?>"></script>
	<script src="<?php echo base_url('mix_assets/js/socket.io.js') ?>"></script>
	<script>
		<?php 
		$level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));

		echo "var id_lokasi = ".$level['id_lokasi'].';';
		?>

		notif_audio = new Audio('<?php echo base_url('mix_assets/sound/notif.mp3') ?>');


		function warning(msg, type = 'custom') {
			//play notif sound
			notif_audio.play();

			//reload table_penanganan_treatment (hanya ada di dashboard)
			if ( typeof tabel_penanganan_treatment !== 'undefined' ) {
			  tabel_penanganan_treatment.ajax.url("dashboard/list_penanganan_treatment").load();
			}
			
			$.notify({
				// options
				// icon: '<?php echo base_url('impressions_asset/Logo_Impressions.png') ?>',
				// title: 'Bootstrap notify',
				message: msg,
				// url: '<?php echo base_url();?>',
				// target: '_blank'
			}, {
				newest_on_top: true,
				allow_dismiss: true,
				//stay terus sampai di dismiss
				delay: 0,
				animate: {
					enter: 'animated fadeInDown',
					exit: 'animated fadeOutUp'
				},
				type: type,
				icon_type: 'img',
			});
		}

		var socket = io.connect('<?php echo getenv("SOCKET_URL").':'.getenv("SOCKET_PORT"); ?>');

		socket.on('pesanan_masuk', function (data) {
			console.log('Pesanan masuk');

			// jika id_lokasi treatment sama dengan id_lokasi login admin / admin yg login adalah admin pusat

			if(data.id_lokasi == id_lokasi || id_lokasi == 1) {
				// beri delay ( 1000 / 1s ) untuk mencegah error di ajax call
				setTimeout(function() {
					$.ajax({
						url : "dashboard/get_sum_data",
						type: "GET",
						dataType: "JSON",
						success: function(data)
						{
							console.log(data);
							
							$('#jumlah_member').text(data.jumlah_member);
							$('#jumlah_pemesanan').text(data.jumlah_pemesanan);
							$('#jumlah_pemesanan_dibayar').text(data.jumlah_pemesanan_dibayar);
							$('#jumlah_produk').text(data.jumlah_produk);
							$('#jumlah_treatment').text(data.jumlah_treatment);
						},
						error: function (jqXHR, textStatus, errorThrown)
						{
							alert('Error get data from ajax');
						}
					}).done(function(){
						warning(data.msg, 'warning');
					});
				}, 1000);
			}
		});

		socket.on('pesanan_dibayarkan', function (data) {
			console.log('Pesanan dibayar');

			// jika id_lokasi treatment sama dengan id_lokasi login admin / admin yg login adalah admin pusat

			if(data.id_lokasi == id_lokasi || id_lokasi == 1) {
				// beri delay ( 1000 / 1s ) untuk mencegah error di ajax call
				setTimeout(function() {
					$.ajax({
						url : "dashboard/get_sum_data",
						type: "GET",
						dataType: "JSON",
						success: function(data)
						{
							console.log(data);
							
							$('#jumlah_member').text(data.jumlah_member);
							$('#jumlah_pemesanan').text(data.jumlah_pemesanan);
							$('#jumlah_pemesanan_dibayar').text(data.jumlah_pemesanan_dibayar);
							$('#jumlah_produk').text(data.jumlah_produk);
							$('#jumlah_treatment').text(data.jumlah_treatment);
						},
						error: function (jqXHR, textStatus, errorThrown)
						{
							alert('Error get data from ajax');
						}
					}).done(function(){
						warning(data.msg, 'success');
					});
				}, 1000);
			}
		});

		function init_sum() {
			
		}
	</script>
</body>
</html>