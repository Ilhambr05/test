<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 28/09/2018
 * Time: 17.37
 */
class Upload_file
{
	function upload_foto($file, $path, $use_tumb, $thumb_path,$with_tumb,$heigh_tumb)
	{
		$target_path 		= $path;
		$tanda				= date("YmdHis");
		$server_ip 			= base_url();
		if (isset($file['name'])) {
			$data = "true";
			if ($file['error'] !== UPLOAD_ERR_OK) {
				$data= "false";
			}

			$info = getimagesize($file['tmp_name']);
			if ($info === FALSE) {
				$data= "false";
			}

			if (($info[2] !== IMAGETYPE_GIF) && ($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
				$data= "false";
			}
			if($data=="true"){
				$imagename 		= $tanda ."_". basename($file['name']);
				$target_path = $target_path . $imagename;
				try {
					if (!move_uploaded_file($file['tmp_name'], $target_path)) {
						$response["ori_image"] = $server_ip."asset/image/noimage.png";
						$response["thumb_image"] = $server_ip."asset/image/noimage.png";
						$response["image_name"] = "noimage.png";
					}else{
						$pathToImages	= $path;
						$thumbWidth 	= $with_tumb;
						$thumbHeight 	= $heigh_tumb;
						if($use_tumb == 1){
							$pathToThumbs 	= $path. 'thumbs/';
							$this->createThumbs($path,$imagename, $pathToThumbs, $thumbWidth ,$thumbHeight);
							$small_pic			= $server_ip . $pathToThumbs . $imagename;
							$response["thumb_image"] = $small_pic;
						}
						$full_pic 			= $server_ip . $target_path;
						$response["ori_image"] = $full_pic;
						$response["image_name"] = $imagename;
					}
				} catch (Exception $e) {
				}
			}else{
				$response["ori_image"] = $server_ip."asset/image/noimage.png";
				$response["thumb_image"] = $server_ip."asset/image/noimage.png";
				$response["image_name"] = "noimage.png";
			}

		} else {
			$response["ori_image"] = $server_ip."asset/image/noimage.png";
			$response["thumb_image"] = $server_ip."asset/image/noimage.png";
			$response["image_name"] = "noimage.png";
		}

		return $response;
	}

	public  function createThumbs($pathToImages,$fname, $pathToThumbs, $thumbWidth ,$thumbHeight) 
	{
		$srcFile = $pathToImages.$fname;
		$thumbFile = $pathToThumbs.$fname;
		if(!empty($fname))
		{
			$type = strtolower(substr( $fname , strrpos( $fname , '.' )+1 ));
			$thumbnail_width=$thumbWidth;
			$thumbnail_height=$thumbHeight;
			switch( $type ){
			case 'jpg' : case 'jpeg' :
			try{
				$src = imagecreatefromjpeg( $srcFile ); break;
			} 
			catch(Exception $e) {
				log_message('System Error', $e->getMessage());
			}
			///$src = imagecreatefromjpeg( $srcFile ); break;
			case 'jpeg' : case 'jpeg' :
			try{
				$src = imagecreatefromjpeg( $srcFile ); break;
			} 
			catch(Exception $e) {
				log_message('System Error', $e->getMessage());
			}
			case 'png' :
			if ( ! function_exists('imagecreatefrompng'))
			{
				$this->set_error(array('imglib_unsupported_imagecreate', 'imglib_png_not_supported'));
				return FALSE;
			}
				$src = imagecreatefrompng( $srcFile ); break;
			case 'gif' :
			if ( ! function_exists('imagecreatefromgif'))
			{
				$this->set_error(array('imglib_unsupported_imagecreate', 'imglib_gif_not_supported'));
				return FALSE;
			}
				$src = imagecreatefromgif( $srcFile ); break;
			}
		list($width_orig, $height_orig) = getimagesize($srcFile); 
		$ratio_orig = $width_orig/$height_orig;
		if ($thumbnail_width/$thumbnail_height > $ratio_orig) {
			$new_height = $thumbnail_width/$ratio_orig;
			$new_width = $thumbnail_width;
		} else {
			$new_width = $thumbnail_height*$ratio_orig;
			$new_height = $thumbnail_height;
		}
		$x_mid = $new_width/2;  //horizontal middle
		$y_mid = $new_height/2; //vertical middle
		$process = imagecreatetruecolor(round($new_width), round($new_height)); 
		imagecopyresampled($process, $src, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
		$dest_img = imagecreatetruecolor($thumbnail_width, $thumbnail_height); 
		imagecopyresampled($dest_img, $process, 0, 0, ($x_mid-($thumbnail_width/2)), ($y_mid-($thumbnail_height/2)), $thumbnail_width, $thumbnail_height, $thumbnail_width, $thumbnail_height);
			switch( $type ){
				case 'jpg' : case 'jpeg' :
					$src = imagejpeg( $dest_img , $thumbFile ); break;
				case 'png' :
				$src = imagepng( $dest_img , $thumbFile ); break;
					case 'gif' :
				$src = imagegif( $dest_img , $thumbFile ); break;
			}
		}
	}
}

?>
