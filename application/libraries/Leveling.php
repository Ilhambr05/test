<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Leveling extends MY_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
    }

    public function cek_level($akses, $return_mode = false)
    {
        $level = $this->session->userdata(getenv("NAME_SESSION_DASHBOARD"));
        // print_r($level);
        $leveling = $level['leveling'];
        // echo "<br>";
        // print_r($leveling);
        // echo "<hr>";
        if(!in_array($akses, $leveling))
        {
            // echo "Redirect ke login";
            if($return_mode){
                return false;
            }
            else{
                redirect('login');
            }
        }else{
            // echo 'lanjut!~~~';
            return true;
        }
    }
}