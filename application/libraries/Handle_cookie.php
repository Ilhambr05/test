<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 28/09/2018
 * Time: 13.42
 */
class Handle_cookie
{
    function process_cookie()
    {
        // fungsi ini digunakan untuk mencocokkan stok / ketersediaan produk pada cookie cart
        // juga sesuaikan lokasi yang dipilih dengan treatment yang tersedia

        // unset($_COOKIE['Impressions-cart']);
        if(isset($_COOKIE['Impressions-cart'])){
            if(sizeof(json_decode($_COOKIE['Impressions-cart']))){
                // print_r($_COOKIE);
                // print_r($this->input->cookie('Impressions-cart', TRUE));
                $cart = json_decode($_COOKIE['Impressions-cart']);
                // echo "<pre>";
                // print_r($cart);
                // echo "</pre><hr>";

                $arr_id = '';
                foreach ($cart as $value) {
                    // print_r($value->id);
                    $arr_id .= $value->id.',';
                }
                $arr_id = rtrim($arr_id,',');//hilangkan koma terakhir
                // echo $arr_id;

                $value = (object) array(
                    'table' => 'produk', //wajib
                    'select' => (object) array( //wajib
                        'string' => "produk.id_produk as id, mp.id_master_produk as id_mp, mp.nama, mp.harga, produk.stok",
                        'no_quotes' => true,
                    ),
                    'join' => array( //opsional
                        (object) array(
                            'join_table' => 'master_produk mp',
                            'on' => 'mp.id_master_produk = produk.id_master_produk',
                        ),
                        (object) array(
                            'join_table' => 'master_jenis mj',
                            'on' => 'mj.id_master_jenis = mp.id_master_jenis',
                        ),
                    ),
                    'where' => array( //opsional
                        (object)array(
                            'one_string' => true,
                            'string' => 'produk.id_produk in ('.$arr_id.')',
                        ),
                        (object) array(
                            'param_name' => 'status',
                            'param_value' => 1,
                        ),
                        (object) array(
                            'param_name' => 'produk.status_delete',
                            'param_value' => 0,
                        ),
                    ),
                );

                $data_db = modules::run('crud/crud/get', $value);
                // print_r($data_db);
                // echo "<br>";

                // print_r($cart[0]);
                // print_r($cart);
                // echo "<br>";

                $data_baru = array();

                //sesuaikan harga dan stok
                foreach ($data_db as $server_data) {
                    $stok_server = $server_data->stok;
                    $id_server = $server_data->id;

                    $index = 0;
                    foreach ($cart as $val_cart) {
                        if($val_cart->id == $id_server){
                            //sesuaikan stok
                            if(isset($val_cart->stok)){
                                if($stok_server < $val_cart->jml){
                                    $val_cart->jml = $stok_server;
                                }
                            }else{
                                $val_cart->stok = $stok_server;
                            }

                            //sesuaikan harga
                            $harga_jadi = $server_data->harga;
                            
                            //jika ada potongan (nominal/persen)
                            // if($server_data->potongan_nominal){
                            //     $harga_jadi = $server_data->harga - $server_data->potongan_nominal;
                            // }
                            // else if($server_data->diskon_persen){
                            //     $harga_jadi = $server_data->harga - ($server_data->harga*$server_data->diskon_persen/100);
                            // }

                            $val_cart->harga = $harga_jadi;

                            //jika stok 0, hilangkan item pesanan
                            if($stok_server < 1){
                                unset($cart[$index]);
                            }else{
                                //masukkan ke data cookie baru
                                array_push($data_baru,$cart[$index]);
                            }

                            break;
                        }

                        $index++;
                    }
                }

                // foreach ($cart as $value) {
                //     array_push($data_baru,$value);
                // }

                // echo "<pre>";
                // print_r($data_baru);
                // echo "</pre><hr>";

                // echo "<pre>";
                // print_r($cart);
                // echo "</pre><hr>";

                // echo "<pre>";
                // print_r(json_encode($cart));
                // echo "</pre><hr>";

                //set cookienya
                // $_COOKIE['Impressions-cart'] = json_encode($cart);

                $ex_time = 7*24*3600 + time(); //ex_time 7 hari;
                setcookie('Impressions-cart', json_encode($data_baru), $ex_time, '/');

                return true;
            }else{
                return false;
            }
        }else{
            // unset($_COOKIE['Impressions-cart']);
            return false;
        }


        //set default lokasi treatment ke id_lokasi pusat
        $id_lokasi = 1;
        // jika member (belum login) sudah memilih lokasi
        if( isset( $_COOKIE['Impressions-cart-lokasi'] ) ) {
            $id_lokasi = $_COOKIE['Impressions-cart-lokasi'];
        }

        // mulai sesuaikan harga dan ketersediaan treatment di lokasi
        if(isset($_COOKIE['Impressions-cart-treatment'])){
            if(sizeof(json_decode($_COOKIE['Impressions-cart-treatment']))){
                // print_r($_COOKIE);
                // print_r($this->input->cookie('Impressions-cart', TRUE));
                $cart = json_decode($_COOKIE['Impressions-cart-treatment']);
                // echo "<pre>";
                // print_r($cart);
                // echo "</pre><hr>";

                $arr_id = '';
                foreach ($cart as $value) {
                    // print_r($value->id);
                    $arr_id .= $value->id.',';
                }
                $arr_id = rtrim($arr_id,',');//hilangkan koma terakhir
                // echo $arr_id;

                $value = (object) array(
                    'table' => 'treatment', //wajib
                    'select' => (object) array( //wajib
                        'string' => "treatment.id_treatment as id, mt.id_master_treatment as id_mt, mt.nama, mt.harga, treatment.id_lokasi",
                        'no_quotes' => true,
                    ),
                    'join' => array( //opsional
                        (object) array(
                            'join_table' => 'master_treatment mt',
                            'on' => 'mt.id_master_treatment = treatment.id_master_treatment',
                        ),
                        (object) array(
                            'join_table' => 'master_jenis mj',
                            'on' => 'mj.id_master_jenis = mt.id_master_jenis',
                        ),
                    ),
                    'where' => array( //opsional
                        (object)array(
                            'one_string' => true,
                            'string' => 'treatment.id_treatment in ('.$arr_id.')',
                        ),
                        (object) array(
                            'param_name' => 'id_lokasi',
                            'param_value' => $id_lokasi,
                        ),
                        (object) array(
                            'param_name' => 'status',
                            'param_value' => 1,
                        ),
                        (object) array(
                            'param_name' => 'treatment.status_delete',
                            'param_value' => 0,
                        ),
                    ),
                );

                $data_db = modules::run('crud/crud/get', $value);
                // print_r($data_db);
                // echo "<br>";

                // print_r($cart[0]);
                // print_r($cart);
                // echo "<br>";

                $data_baru = array();

                //sesuaikan harga
                foreach ($data_db as $server_data) {
                    $stok_server = $server_data->stok;
                    $id_server = $server_data->id;

                    $index = 0;
                    foreach ($cart as $val_cart) {
                        //hanya pilih id_treatment yang sesuai hasil query
                        if($val_cart->id == $id_server){

                            //sesuaikan harga
                            $harga_jadi = $server_data->harga;

                            $val_cart->harga = $harga_jadi;

                            //masukkan ke data cookie baru
                            array_push($data_baru,$cart[$index]);

                            break;
                        }

                        $index++;
                    }
                }

                $ex_time = 7*24*3600 + time(); //ex_time 7 hari;
                setcookie('Impressions-cart-treatment', json_encode($data_baru), $ex_time, '/');

                return true;
            }else{
                return false;
            }
        }else{
            // unset($_COOKIE['Impressions-cart']);
            return false;
        }
    }
}
?>