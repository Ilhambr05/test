<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 28/09/2018
 * Time: 13.42
 */
class Date
{
    public function getDateFull()
    {
        date_default_timezone_set('Asia/Jakarta');
        $create_at = date("Y-m-d H:i:s");
        return $create_at;
    }

    public function getDate()
    {
        date_default_timezone_set('Asia/Jakarta');
        $create_at = date("Y-m-d");
        return $create_at;
    }

    public function tanggal_indo($tanggal, $cetak_hari = false)
    {
        $hari = array(1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu',
        );

        $bulan = array(1 => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember',
        );

        $tgl_indo = ' - ';
        if($tanggal && $tanggal !== '0000-00-00'){
            $split = explode('-', $tanggal);
            // print_r($split);
            $tgl_indo = $split[2] . ' ' . $bulan[(int) $split[1]] . ' ' . $split[0];
        }

        if ($cetak_hari) {
            $num = date('N', strtotime($tanggal));
            return $hari[$num] . ', ' . $tgl_indo;
        }
        return $tgl_indo;
    }
}
